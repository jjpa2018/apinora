<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['middleware'=> 'auth','prefix' => 'api/v1'], function () use ($router) {

    /* Questionnaries */
    $router->post('/c0','C0BotController@store');
    $router->post('/c1', 'C1BotController@store');
    $router->post('/c2', 'C2BotController@store');
    $router->post('/c3', 'C3BotController@store');
    $router->post('/c4a1','C4a1BotController@store');
    $router->post('/c4a2','C4a2BotController@store');
    $router->post('/c4b1','C4b1BotController@store');
    $router->post('/c4b2','C4b2BotController@store');
    $router->post('/c4b3','C4b3BotController@store');
    $router->post('/cr1', 'Cr1BotController@store');
    $router->post('/cr1b', 'Cr1bController@store');
    $router->post('/cr2', 'Cr2BotController@store');
    $router->post('/cr2b', 'Cr2bController@store');
    $router->post('/c5', 'C5BotController@store');

});

$router->group(['middleware'=> 'auth','prefix' => 'api/notifica'], function () use ($router) {

    /* QNotificaciones Y recordatorios Nora*/
    $router->post('/dayly','RecordatorioVacunaController@actionDaily');

});

$router->group(['middleware'=> 'auth','prefix' => 'api/cuadromando'], function () use ($router) {

    /* Consultas para indicadores */
    $router->get('/genero','CuadroMandoController@genero');
    $router->get('/grupo','CuadroMandoController@grupoEdad');
    $router->get('/demo','CuadroMandoController@demografico');
    $router->get('/indicadores','CuadroMandoController@indicadores');
    $router->get('/data','CuadroMandoController@dataCuestionarios');
    $router->get('/cuestionario','CuadroMandoController@cuestionariosExport');
    $router->get('/vacuna','consultasGeneralesController@consultaVacuna');
    $router->get('/img','CuadroMandoController@pruebaImagen');
    $router->get('/loadImg','CuadroMandoController@EndpointImage');
});

$router->get('/imagen/{id}','CuadroMandoController@downloadImage');

