<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DireccionPaciente extends Model{
    protected $table = "direccion_paciente";

    protected $fillable = ['idpersona','provincia','distrito','corregimiento','poblado','direccion'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}