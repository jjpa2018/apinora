<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SintomasR1a extends Model{
    protected $table = "sintomasUserCr1a";

    protected $fillable = ['sintoma','fecha_inicio','fecha_fin','mejoro','intensidad','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}