<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReactogeneidadUser extends Model{
    protected $table = "rectogeneidad_users";

    protected $fillable = ['idpersona','grupo','fecha','estatus'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}