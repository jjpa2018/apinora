<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DosisPersona extends Model{
    protected $table = "dosisusuario";

    protected $fillable = ['dosis','idpersona','fecha','estatus'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}