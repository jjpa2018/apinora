<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SintomasC4a1 extends Model{
    protected $table = "sintomasUserC4a1";

    protected $fillable = ['sintoma','fecha_inicio','fecha_fin','mejoro','intensidad','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}