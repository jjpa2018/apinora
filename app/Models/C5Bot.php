<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class C5Bot extends Model{
    protected $table = "c5-bot";

    protected $fillable = ['C5_1','C5_2','C5_3','C5_3_1','C5_3_1_1','C5_3_2','C5_3_2_1','C5_3_3','C5_3_3_1','C5_4','C5_5','C5_6','C5_6_1','C5_6_2','C5_7','C5_7_1_A','C5_7_1_B','C5_7_1_C','C5_7_1_D','C5_7_1_E','C5_7_1','C5_7_2','C5_8','C5_8_1','C5_9','C5_9_1','C5_9_2','C5_10','C5_10_1','C5_11','C5_12','C5_12_1','C5_13','C5_13_1','C5_13_1_1','C5_13_1_2','C5_13_2','C5_13_2_1','C5_13_2_2','C5_13_3','C5_13_3_1','C5_13_3_2','C5_13_4','C5_13_4_1','C5_13_4_2','C5_14','C5_14_1','C5_14_2','C5_15','fecha_operacion','hora','idpersona'];   

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
} 