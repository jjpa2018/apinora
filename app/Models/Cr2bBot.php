<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cr2bBot extends Model{
    protected $table = "cr2b-bot";

    protected $fillable = ['CR2B_1','CR2B_2','CR2B_3','CR2B_3_1','CR2B_3_1_1','CR2B_3_2','CR2B_3_2_1','CR2B_3_3','CR2B_3_3_1','CR2B_4','CR2B_5','CR2B_6','CR2B_6_1','CR2B_6_2','CR2B_7','CR2B_7_1','CR2B_8','CR2B_8_1_A','CR2B_8_1_B','CR2B_8_1_C','CR2B_8_1_D','CR2B_8_1_E','CR2B_8_2','CR2B_9','CR2B_9_1','CR2B_10','CR2B_11','CR2B_11_1','CR2B_11_2','CR2B_12','CR2B_12_1','CR2B_13','CR2B_13_1','CR2B_13_2','CR2B_14','CR2B_14_1','fecha_operacion','hora','idpersona'];   

    // public $timestamps = false; 

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
    // public $timestamps = false;
}