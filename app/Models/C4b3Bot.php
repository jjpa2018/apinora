<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class C4b3Bot extends Model{
    protected $table = "c4b3-bot";

    protected $fillable = ['C4B3_1','C4B3_2','C4B3_3','C4B3_3_1','C4B3_3_2','C4B3_4','C4B3_4_1_A','C4B3_4_1_B','C4B3_4_1_C','C4B3_4_1_D','C4B3_4_1_E','C4B3_4_2','C4B3_5','C4B3_5_1','C4B3_6','C4B3_6_1','C4B3_6_2','C4B3_7','C4B3_7_1','C4B3_8','C4B3_9','C4B3_9_1','C4B3_10','C4B3_10_1','C4B3_10_1_1','C4B3_10_1_2','C4B3_10_2','C4B3_10_2_1','C4B3_10_2_2','C4B3_10_3','C4B3_10_3_1','C4B3_10_3_2','C4B3_10_4','C4B3_10_4_1','C4B3_10_4_2','C4B3_11','C4B3_11_1','C4B3_11_2','C4B3_12','fecha_operacion','hora','idpersona'];   

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
} 