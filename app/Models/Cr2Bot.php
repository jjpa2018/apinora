<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cr2Bot extends Model{
    protected $table = "cr2-bot";

    protected $fillable = ['CR2_3','CR2_4','CR2_4_1','CR2_4_2','CR2_5','CR2_5_1_1','CR2_5_2_1','CR2_5_3_1','CR2_5_4_1','CR2_5_5_1','CR2_5_1_2','CR2_5_2_2','CR2_5_3_2','CR2_5_4_2','CR2_5_5_2','CR2_5_1_3','CR2_5_2_3','CR2_5_3_3','CR2_5_4_3','CR2_5_5_3','CR2_5_1_4','CR2_5_2_4','CR2_5_3_4','CR2_5_4_4','CR2_5_5_4','CR2_5_1_5','CR2_5_2_5','CR2_5_3_5','CR2_5_4_5','CR2_5_5_5','CR2_5_1_6','CR2_5_2_6','CR2_5_3_6','CR2_5_4_6','CR2_5_5_6','CR2_5_1_7','CR2_5_2_7','CR2_5_3_7','CR2_5_4_7','CR2_5_5_7','CR2_5_1_8','CR2_5_2_8','CR2_5_3_8','CR2_5_4_8','CR2_5_5_8','CR2_5_1_9','CR2_5_2_9','CR2_5_3_9','CR2_5_4_9','CR2_5_5_9','CR2_5_1_10','CR2_5_2_10','CR2_5_3_10','CR2_5_4_10','CR2_5_5_10','CR2_5_1_11','CR2_5_2_11','CR2_5_3_11','CR2_5_4_11','CR2_5_5_11','CR2_5_1_12','CR2_5_2_12','CR2_5_3_12','CR2_5_4_12','CR2_5_5_12','CR2_5_1_13','CR2_5_2_13','CR2_5_3_13','CR2_5_4_13','CR2_5_5_13','CR2_5_1_14','CR2_5_2_14','CR2_5_3_14','CR2_5_4_14','CR2_5_5_14','CR2_5_1_15','CR2_5_2_15','CR2_5_3_15','CR2_5_4_15','CR2_5_5_15','CR2_5_1_16','CR2_5_2_16','CR2_5_3_16','CR2_5_4_16','CR2_5_5_16','CR2_5_1_17','CR2_5_2_17','CR2_5_3_17','CR2_5_4_17','CR2_5_5_17','CR2_5_1_18','CR2_5_2_18','CR2_5_3_18','CR2_5_4_18','CR2_5_5_18','CR2_5_1_19','CR2_5_2_19','CR2_5_3_19','CR2_5_4_19','CR2_5_5_19','CR2_5_1_20','CR2_5_2_20','CR2_5_3_20','CR2_5_4_20','CR2_5_5_20','CR2_5_1_21','CR2_5_2_21','CR2_5_3_21','CR2_5_4_21','CR2_5_5_21','CR2_5_1_22','CR2_5_2_22','CR2_5_3_22','CR2_5_4_22','CR2_5_5_22','CR2_5_1_23','CR2_5_2_23','CR2_5_3_23','CR2_5_4_23','CR2_5_5_23','CR2_5_1_24','CR2_5_2_24','CR2_5_3_24','CR2_5_4_24','CR2_5_5_24','CR2_6','CR2_6_1','CR2_6_2','CR2_7','CR2_8','CR2_8_1','CR2_8_2','CR2_9','CR2_9_1','CR2_9_1_A','CR2_9_1_','CR2_9_1_','CR2_9_1_','CR2_9_1_E','CR2_9_2','CR2_10','CR2_10_1','CR2_10_2','CR2_11','CR2_11_1','CR2_12','CR2_13','CR2_13_1','CR2_13_2','CR2_14','CR2_14_1','fecha_operacion','hora','idpersona'];   

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
} 