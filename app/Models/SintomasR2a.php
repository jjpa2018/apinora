<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SintomasR2a extends Model{
    protected $table = "sintomasUserCr2";

    protected $fillable = ['sintoma','fecha_inicio','fecha_fin','mejoro','intensidad','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}