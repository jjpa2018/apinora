<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CieTutorBot extends Model{
    protected $table = "c0-tutor";

    protected $fillable = ['cedula_panama','cedula_informante','nombre_representante','fecha_nacimiento_r','prentesco','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}