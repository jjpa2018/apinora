<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultaProfUser extends Model{
    protected $table = "consulta_prof_user";

    protected $fillable = ['medio','dosis','idpersona','cuestionario'];
    
    // public $timestamps = false;
    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
} 