<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicamentoPostVacuna extends Model{
    protected $table = "medicamento_post_vacuna_user";

    protected $fillable = ['idpersona','medicamento','uso','recomendado_por','cuestionario'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}