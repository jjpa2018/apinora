<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class C0Bot extends Model{
    protected $table = "c0-bot";

    protected $fillable = ['C0_1','C0_1_1','C0_1_2','C0_1_3','C0_1_3_1','C0_1_4','C0_2','C0_2_1','C0_2_2','C0_3','C0_4','C0_5','C0_5_1','C0_5_2','C0_6','C0_7','C0_8','C0_8_1','idpersona','fecha_operacion','hora'];
    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}