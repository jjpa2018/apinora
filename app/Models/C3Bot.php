<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class C3Bot extends Model{
    protected $table = "c3-bot";

    protected $fillable = ['C3_1','C3_2','C3_3','C3_3_1','C3_4','C3_5','C3_5_1','C3_6','C3_6_1','C3_7','C3_8','C3_9','C3_9_1','C3_9_2','fecha_operacion','hora','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}