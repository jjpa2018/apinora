<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnfermedadPaciente extends Model{
    protected $table = "enfermedad_paciente";

    protected $fillable = ['cod_preg_encuesta','respuesta','fecha_operacion','hora','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}