<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class C4b2Bot extends Model{
    protected $table = "c4b2-bot";

    protected $fillable = ['C4B2_1','C4B2_2','C4B2_3','C4B2_4','C4B2_5','C4B2_6','C4B2_6_1','C4B2_6_2','C4B2_7','C4B2_7_1_A','C4B2_7_1_B','C4B2_7_1_C','C4B2_7_1_D','C4B2_7_1_E','C4B2_7_2','C4B2_8','C4B2_8_1','C4B2_9','C4B2_9_1','C4B2_9_2','C4B2_10','C4B2_10_1','C4B2_11','C4B2_12','C4B2_12_1','C4B2_13','C4B2_13_1','C4B2_13_1_1','C4B2_13_1_2','C4B2_13_2','C4B2_13_2_1','C4B2_13_2_2','C4B2_13_3','C4B2_13_3_1','C4B2_13_3_2','C4B2_13_4','C4B2_13_4_1','C4B2_13_4_2','C4B2_14','C4B2_14_1','C4B2_14_2','C4B2_15','fecha_operacion','hora','idpersona'];   

    // public $timestamps = false;

    public function Persona() 
    {
        return $this->belongsTo('App\Models\Persona');
    }
}