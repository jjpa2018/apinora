<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SintomasC1b extends Model{
    protected $table = "sintomasUserc1b";

    protected $fillable = ['sintoma','fecha_inicio','fecha_fin','mejoro','intensidad','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}