<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cr1bBot extends Model{
    protected $table = "cr1b-bot";

    protected $fillable = ['CR1B_1','CR1B_2','CR1B_3','CR1B_3.1','CR1B_3.1.1','CR1B_3.2','CR1B_3.2.1','CR1B_3.3','CR1B_3.3.1','CR1B_4','CR1B_5','CR1B_6','CR1B_6.1','CR1B_6.2','CR1B_7','CR1B_7.1','CR1B_8','CR1B_8.1_A','CR1B_8.1_B','CR1B_8.1_C','CR1B_8.1_D','CR1B_8.1_E','CR1B_8.2','CR1B_9','CR1B_9.1','CR1B_10','CR1B_11','CR1B_11.1','CR1B_11.2','CR1B_12','CR1B_12.1','CR1B_13','CR1B_13.1','CR1B_13.2','CR1B_14','CR1B_14.1','fecha_operacion','hora','idpersona'];   

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
    // public $timestamps = false;
} 