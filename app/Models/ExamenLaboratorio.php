<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamenLaboratorio extends Model{
    protected $table = "examenLabUser";

    protected $fillable = ['cod_preg_encuesta','examen','idpersona','cuestionario'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}