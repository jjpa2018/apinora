<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class C4a2Bot extends Model{
    protected $table = "c4a2-bot";

    protected $fillable = ['C4A2_1','C4A2_2','C4A2_3','C4A2_3_1','C4A2_3_1_1','C4A2_3_2','C4A2_3_2_1','C4A2_3_3','C4A2_3_3_1','C4A2_4','C4A2_5','C4A2_6','C4A2_6_1','C4A2_6_2','C4A2_7','C4A2_7_1_A','C4A2_7_1_B','C4A2_7_1_C','C4A2_7_1_D','C4A2_7_1_E','C4A2_7_2','C4A2_8','C4A2_8_1','C4A2_9','C4A2_9_1','C4A2_9_2','C4A2_10','C4A2_10_1','C4A2_11','C4A2_12','C4A2_12_1','C4A2_13','C4A2_13_1','C4A2_13_1_1','C4A2_13_1_2','C4A2_13_2','C4A2_13_2_1','C4A2_13_2_2','C4A2_13_3','C4A2_13_3_1','C4A2_13_3_2','C4A2_13_4','C4A2_13_4_1','C4A2_13_4_2','C4A2_14','C4A2_14_1','C4A2_14_2','C4A2_15','fecha_operacion','hora','idpersona'];    

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
} 