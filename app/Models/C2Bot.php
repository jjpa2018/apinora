<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class C2Bot extends Model{
    protected $table = "c2-bot";

    protected $fillable = ['C2_1','C2_2','C2_2_3','C2_5','C2_5_1','C2_6','C2_6_1','C2_7','C2_7_1','C2_8','C2_8_1','C2_9','C2_9_1','C2_10','C2_10_1','C2_11','C2_11_1','C2_12','C2_12_1','C2_12_2','fecha_operacion','hora','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}