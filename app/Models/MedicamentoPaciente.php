<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicamentoPaciente extends Model{
    protected $table = "medicamento_paciente";

    protected $fillable = ['idpersona','medicamento','uso'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}