<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuestionarioPorUsuario extends Model{
    protected $table = "cuestionario_por_usuario";

    protected $fillable = ['idpersona','cuestionario','fecha','hora','estatus'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}