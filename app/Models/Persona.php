<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model{
    protected $table = "persona";

    protected $fillable = ['tiene_pasaporte','doc_identifica','cod_unico','telefono','conector_pau','correo_informante','nombre_participante','fecha_nacimiento','edad','grupo','estatus'];

// public $timestamps = false;


    //relacion uno mucho con los demas cuestionarios

    public function C0Bots()
    {
        return $this->hasMany('App\Models\C0Bot');
    }

    public function C1Bots()
    {
        return $this->hasMany('App\Models\C1Bot');
    }

    public function C2Bots()
    {
        return $this->hasMany('App\Models\C2Bot');
    }

    public function C3Bots()
    {
        return $this->hasMany('App\Models\C3Bot');
    }

    public function C4a1Bots()
    {
        return $this->hasMany('App\Models\C4a1Bot');
    }

    public function C4a2Bots()
    {
        return $this->hasMany('App\Models\C4a2Bot');
    }

    public function C4b1Bots()
    {
        return $this->hasMany('App\Models\C4b1Bot');
    }

    public function C4b2Bots()
    {
        return $this->hasMany('App\Models\C4b2Bot');
    }

    public function C4b3Bots()
    {
        return $this->hasMany('App\Models\C4b3Bot');
    }

    public function C5Bots()
    {
        return $this->hasMany('App\Models\C5Bot');
    }

    public function Cr1Bots()
    {
        return $this->hasMany('App\Models\Cr1Bot');
    }

    public function Cr2Bots()
    {
        return $this->hasMany('App\Models\Cr2Bot');
    }

    public function CuestionarioPorUsuarios()
    {
        return $this->hasMany('App\Models\CuestionarioPorUsuario');
    }

    public function EnfermedadPacientes()
    {
        return $this->hasMany('App\Models\EnfermedadPaciente');
    }

    public function MedicamentoPacientes()
    {
        return $this->hasMany('App\Models\MedicamentoPaciente');
    }

    public function ReactogeneidadUsers()
    {
        return $this->hasMany('App\Models\ReactogeneidadUser');
    }

    public function CieTutorBots()
    {
        return $this->hasMany('App\Models\CieTutorBot');
    }

    public function SintomasC1b()
    {
        return $this->hasMany('App\Models\SintomasC1b');
    }

    public function SintomasC4a1()
    {
        return $this->hasMany('App\Models\SintomasC4a1');
    }

    public function SintomasR1a()
    {
        return $this->hasMany('App\Models\SintomasR1a');
    }

    public function SintomasR2a()
    {
        return $this->hasMany('App\Models\SintomasR2a');
    }

    public function ExamenLaboratorio()
    {
        return $this->hasMany('App\Models\ExamenLaboratorio');
    }

    public function MedicamentoPostVacuna()
    {
        return $this->hasMany('App\Models\MedicamentoPostVacuna');
    }

    public function ReincidenciaCovidUser()
    {
        return $this->hasMany('App\Models\ReincidenciaCovidUser');
    }

    public function ConsultaProfuser()
    {
        return $this->hasMany('App\Models\ConsultaProfuser');
    }

    public function DireccionPaciente()
    {
        return $this->hasMany('App\Models\DireccionPaciente');
    }
    
}