<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class C1Bot extends Model{
    protected $table = "c1-bot";

    protected $fillable = ['C1_1','C1_1_1','C1_3','C1_3_1','C1_3_2','C1_3_3','C1_4','C1_4_1','C1_5','C1_5_1','C1_5_2','C1_5_3','C1_5_4','C1_6','C1_7','C1_8','C1_9','C1_9_1','C1_10','C1_10_1','C1_11','C1_11_1_A','C1_11_1_B','C1_11_1_C','C1_11_1_D','C1_11_1_E','C1_11_1_F','C1_11_1_G','C1_11_1_H','C1_11_1_I','C1_11_1_J','C1_11_1_K','C1_11_1_L','C1_12','C1_13','C1_14','C1_15','C1_17','C1_18','C1_19','C1_20','C1_21','C1_21_1','C1_22','C1_22_1','C1_23','C1_23_1','C1_23_2','C1_24','C1_24_1_1','C1_24_1_1_1','C1_24_1_2','C1_24_1_2_1','C1_24_1_3','C1_24_1_3_1','C1_24_1_4','C1_24_1_4_1','C1_25','C1_25_1','C1_25_2','C1_25_3','C1_25_4','C1_25_5','fecha_operacion','hora','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}