<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReincidenciaCovidUser extends Model{
    protected $table = "reincide_covid_user";

    protected $fillable = ['recuerda_fecha','fecha_prueba','cuestionario','dosis','idpersona'];

    // public $timestamps = false;

    public function Persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }
}