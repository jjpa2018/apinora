<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\C4b3Bot;
use App\Models\ConsultaProfUser;
use Illuminate\Http\Request;
use App\Models\CuestionarioPorUsuario;
use App\Models\ExamenLaboratorio;
use App\Models\MedicamentoPostVacuna;
use App\Models\ReincidenciaCovidUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class C4b3BotController extends Controller{
	public function store(Request $request)
    {
        try{
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);
            $examenLab = Resources::ExamenLab($respuestas['C4B3_4.1']);
            $positivo =$respuestas['C4B3_11']; 
            $fecha_aprox_prueba = $respuestas['C4B3_11.1'];
            isset($respuestas['C4B3_11.2'])?$fecha_prueba=$respuestas['C4B3_11.2']:$fecha_prueba='';
            $consulta = $respuestas['C4B3_9'];
            $consulta == 'Si'?$medio= Resources::Medio(Resources::QuitaAcento($respuestas['C4B3_9.1'])):$medio='';

            if (!empty(array_filter($respuestas['C4B3_10.1']))) {
                $medicamento = array_filter($respuestas['C4B3_10.1']['medicamento']);           
                $uso = array_filter($respuestas['C4B3_10.1']['uso']); 
                $recetado_por = array_filter($respuestas['C4B3_10.1']['receto']); 
            }
            else{
                $medicamento = '';
                $uso = '';
                $recetado_por = '';
            }

            if ($id_persona != 0) 
            {
                DB::beginTransaction();
                try {
                        $C4b3 = C4b3Bot::create([
                            'C4B3_1'=>$respuestas['C4B3_1'],
                            'C4B3_2'=>$respuestas['C4B3_2'],
                            'C4B3_3'=>$respuestas['C4B3_3'],
                            'C4B3_3_1'=>$respuestas['C4B3_3.1'],
                            'C4B3_3_2'=>$respuestas['C4B3_3.2'],
                            'C4B3_4'=>$respuestas['C4B3_4'],
                            'C4B3_4_1_A'=>$respuestas['C4B3_4.1']['C4B3_4.1_A'],
                            'C4B3_4_1_B'=>$respuestas['C4B3_4.1']['C4B3_4.1_B'],
                            'C4B3_4_1_C'=>$respuestas['C4B3_4.1']['C4B3_4.1_C'],
                            'C4B3_4_1_D'=>$respuestas['C4B3_4.1']['C4B3_4.1_D'],
                            'C4B3_4_1_E'=>$respuestas['C4B3_4.1']['C4B3_4.1_E'],
                            'C4B3_4_2'=>$respuestas['C4B3_4.2'],
                            'C4B3_5'=>$respuestas['C4B3_5'],
                            'C4B3_5_1'=>$respuestas['C4B3_5.1'],
                            'C4B3_6'=>$respuestas['C4B3_6'],
                            'C4B3_6_1'=>$respuestas['C4B3_6.1'],
                            'C4B3_6_2'=>$respuestas['C4B3_6.2'],
                            'C4B3_7'=>$respuestas['C4B3_7'],
                            'C4B3_7_1'=>$respuestas['C4B3_7.1'],
                            'C4B3_8'=>$respuestas['C4B3_8'],
                            'C4B3_9'=>$respuestas['C4B3_9'],
                            'C4B3_9_1'=>$respuestas['C4B3_9.1'],
                            'C4B3_10'=>$respuestas['C4B3_10'],
                            'C4B3_10_1'=>$respuestas['C4B3_10.1']['medicamento']['C4B3_10.1'],
                            'C4B3_10_1_1'=>$respuestas['C4B3_10.1']['uso']['C4B3_10.1'],
                            'C4B3_10_1_2'=>$respuestas['C4B3_10.1']['receto']['C4B3_10.1'],
                            'C4B3_10_2'=>$respuestas['C4B3_10.1']['medicamento']['C4B3_10.2'],
                            'C4B3_10_2_1'=>$respuestas['C4B3_10.1']['uso']['C4B3_10.2'],
                            'C4B3_10_2_2'=>$respuestas['C4B3_10.1']['receto']['C4B3_10.2'],
                            'C4B3_10_3'=>$respuestas['C4B3_10.1']['medicamento']['C4B3_10.3'],
                            'C4B3_10_3_1'=>$respuestas['C4B3_10.1']['uso']['C4B3_10.3'],
                            'C4B3_10_3_2'=>$respuestas['C4B3_10.1']['receto']['C4B3_10.3'],
                            'C4B3_10_4'=>$respuestas['C4B3_10.1']['medicamento']['C4B3_10.4'],
                            'C4B3_10_4_1'=>$respuestas['C4B3_10.1']['uso']['C4B3_10.4'],
                            'C4B3_10_4_2'=>$respuestas['C4B3_10.1']['receto']['C4B3_10.4'],
                            'C4B3_11'=>$respuestas['C4B3_11'],
                            'C4B3_11_1'=>$respuestas['C4B3_11.1'],
                            'C4B3_11_2'=>$respuestas['C4B3_11.2'],
                            'C4B3_12'=>$respuestas['C4B3_12'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                        $C4b3->save();

                    if (!empty($examenLab)) 
                        {
                            foreach ($examenLab as $key => $value) {
                                $examenLab = ExamenLaboratorio::create([
                                    'cod_preg_encuesta' => $key,
                                    'examen' => $value,
                                    'idpersona' => $id_persona,
                                    'cuestionario' => $n_question
                                ]);
                                $examenLab->save();
                            }
                        }

                        if (!empty($positivo == 'Si')) 
                        {
                            $reincide = ReincidenciaCovidUser::create([
                                'recuerda_fecha' => $fecha_aprox_prueba,
                                'fecha_prueba' => $fecha_prueba,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question,
                                'dosis' => '1'
                            ]);
                            $reincide->save();
                        }

                        if (!empty($consulta == 'Si')) 
                        {
                            $prof = ConsultaProfUser::create([
                                'medio' => $medio,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question,
                                'dosis' => '1'
                            ]);
                            $prof->save();
                        }

                        if (!empty($medicamento) && !empty($uso)) 
                        {
                                $cantMedi = count($medicamento);
                                $cantUso = count($uso);
                                if($cantMedi == $cantUso)
                                {
                                    $cantidad = $cantMedi;
                                    for ($i=1; $i <= $cantidad; $i++) 
                                    { 
                                        $medicina = MedicamentoPostVacuna::create([
                                            'idpersona' => $id_persona,
                                            'medicamento' => $medicamento['C4B3_10.'.$i],
                                            'uso' => $uso['C4B3_10.'.$i],
                                            'recomendado_por' => $recetado_por['C4B3_10.'.$i],
                                            'cuestionario' => $n_question
                                        ]);
                                    }
                                    $medicina->save();
                                }
                        }

                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }else{
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }

        }catch(\Exception $e) {            
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }
    }
}