<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\C3Bot;
use Illuminate\Http\Request;
use App\Models\CuestionarioPorUsuario;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class C3BotController extends Controller{

    public function store(Request $request)
    {
        try{
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);

            if ($id_persona != 0) 
            {
                DB::beginTransaction();
                try {

                        $c3 = C3Bot::create([
                            'C3_1'=>$respuestas['C3.1'],
                            'C3_2'=>$respuestas['C3.2'],
                            'C3_3'=>$respuestas['C3.3'],
                            'C3_3_1'=>$respuestas['C3.3.1'],
                            'C3_4'=>$respuestas['C3.4'],
                            'C3_5'=>$respuestas['C3.5'],
                            'C3_5_1'=>$respuestas['C3.5.1'],
                            'C3_6'=>$respuestas['C3.6'],
                            'C3_6_1'=>$respuestas['C3.6.1'],
                            'C3_7'=>$respuestas['C3.7'],
                            'C3_8'=>$respuestas['C3.8'],
                            'C3_9'=>$respuestas['C3.9'],
                            'C3_9_1'=>$respuestas['C3.9.1'],
                            'C3_9_2'=>$respuestas['C3.9.2'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                        $c3->save();

                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }
            else
            {
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }

        }catch(\Exception $e) {            
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }
    }
}