<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\ConsultaProfUser;
use App\Models\Cr2bBot;
use App\Models\CuestionarioPorUsuario;
use App\Models\ExamenLaboratorio;
use App\Models\ReincidenciaCovidUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Cr2bController extends Controller{

    public function store(Request $request)
    {
        try{
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);
            $examenLab = Resources::ExamenLab($respuestas['CR2B_8.1']);

            $positivo =$respuestas['CR2B_13']; 
            $fecha_aprox_prueba = $respuestas['CR2B_13.1'];
            isset($respuestas['CR2B_13.2'])?$fecha_prueba=$respuestas['CR2B_13.2']:$fecha_prueba='';
            $consulta = $respuestas['CR2B_14'];
            $consulta == 'Si'?$medio= Resources::Medio(Resources::QuitaAcento($respuestas['CR2B_14.1'])):$medio='';

            if ($id_persona != 0)  
            {
                DB::beginTransaction();
                try {
                        $Cr2b = Cr2bBot::create([
                            'CR2B_1'=>$respuestas['CR2B_1'],
                            'CR2B_2'=>$respuestas['CR2B_2'],
                            'CR2B_3'=>$respuestas['CR2B_3'],
                            'CR2B_3.1'=>$respuestas['CR2B_3.1'],
                            'CR2B_3.1.1'=>$respuestas['CR2B_3.1.1'],
                            'CR2B_3.2'=>$respuestas['CR2B_3.2'],
                            'CR2B_3.2.1'=>$respuestas['CR2B_3.2.1'],
                            'CR2B_3.3'=>$respuestas['CR2B_3.3'],
                            'CR2B_3.3.1'=>$respuestas['CR2B_3.3.1'],
                            'CR2B_4'=>$respuestas['CR2B_4'],
                            'CR2B_5'=>$respuestas['CR2B_5'],
                            'CR2B_6'=>$respuestas['CR2B_6'],
                            'CR2B_6.1'=>$respuestas['CR2B_6.1'],
                            'CR2B_6.2'=>$respuestas['CR2B_6.2'],
                            'CR2B_7'=>$respuestas['CR2B_7'],
                            'CR2B_7.1'=>$respuestas['CR2B_7.1'],
                            'CR2B_8'=>$respuestas['CR2B_8'],
                            'CR2B_8.1_A'=>$respuestas['CR2B_8.1']['CR2B_8.1_A'],
                            'CR2B_8.1_B'=>$respuestas['CR2B_8.1']['CR2B_8.1_B'],
                            'CR2B_8.1_C'=>$respuestas['CR2B_8.1']['CR2B_8.1_C'],
                            'CR2B_8.1_D'=>$respuestas['CR2B_8.1']['CR2B_8.1_D'],
                            'CR2B_8.1_E'=>$respuestas['CR2B_8.1']['CR2B_8.1_E'],
                            'CR2B_8.2'=>$respuestas['CR2B_8.2'],
                            'CR2B_9'=>$respuestas['CR2B_9'],
                            'CR2B_9.1'=>$respuestas['CR2B_9.1'],
                            'CR2B_10'=>$respuestas['CR2B_10'],
                            'CR2B_11'=>$respuestas['CR2B_11'],
                            'CR2B_11.1'=>$respuestas['CR2B_11.1'],
                            'CR2B_11.2'=>$respuestas['CR2B_11.2'],
                            'CR2B_12'=>$respuestas['CR2B_12'],
                            'CR2B_12.1'=>$respuestas['CR2B_12.1'],
                            'CR2B_13'=>$respuestas['CR2B_13'],
                            'CR2B_13.1'=>$respuestas['CR2B_13.1'],
                            'CR2B_13.2'=>$respuestas['CR2B_13.2'],
                            'CR2B_14'=>$respuestas['CR2B_14'],
                            'CR2B_14.1'=>$respuestas['CR2B_14.1'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                        $Cr2b->save();

                    if (!empty($examenLab)) 
                        {
                            foreach ($examenLab as $key => $value) {
                                $examenLab = ExamenLaboratorio::create([
                                    'cod_preg_encuesta' => $key,
                                    'examen' => $value,
                                    'idpersona' => $id_persona,
                                    'cuestionario' => $n_question
                                ]);
                                $examenLab->save();
                            }
                        }

                        if (!empty($positivo == 'Si')) 
                        {
                            $reincide = ReincidenciaCovidUser::create([
                                'recuerda_fecha' => $fecha_aprox_prueba,
                                'fecha_prueba' => $fecha_prueba,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question,
                                'dosis' => '1'
                            ]);
                            $reincide->save();
                        }

                        if (!empty($consulta == 'Si')) 
                        {
                            $prof = ConsultaProfUser::create([
                                'medio' => $medio,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question,
                                'dosis' => '1'
                            ]);
                            $prof->save();
                        }

                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }else{
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }

        }catch(\Exception $e) {            
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }
    }

}