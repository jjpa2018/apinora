<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\C5Bot;
use App\Models\ConsultaProfUser;
use Illuminate\Http\Request;
use App\Models\CuestionarioPorUsuario;
use App\Models\ExamenLaboratorio;
use App\Models\MedicamentoPostVacuna;
use App\Models\ReincidenciaCovidUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class C5BotController extends Controller{

	public function store(Request $request)
    {
        try{
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);
            $examenLab = Resources::ExamenLab($respuestas['C5_7.1']);
            $positivo =$respuestas['C5_14']; 
            $fecha_aprox_prueba = $respuestas['C5_14.1'];
            isset($respuestas['C5_14.2'])?$fecha_prueba=$respuestas['C5_14.2']:$fecha_prueba='';
            $consulta = $respuestas['C5_12'];
            $consulta == 'Si'?$medio= Resources::Medio(Resources::QuitaAcento($respuestas['C5_12.1'])):$medio='';
 
            if (!empty(array_filter($respuestas['C5_13.1']))) {
                $medicamento = array_filter($respuestas['C5_13.1']['medicamento']);           
                $uso = array_filter($respuestas['C5_13.1']['uso']); 
                $recetado_por = array_filter($respuestas['C5_13.1']['receto']); 
            }
            else{
                $medicamento = '';
                $uso = '';
                $recetado_por = '';
            }

            if ($id_persona != 0) 
            {
                DB::beginTransaction();
                try {
                        $c5 = C5Bot::create([
                            'C5_1'=>$respuestas['C5_1'],
                            'C5_2'=>$respuestas['C5_2'],
                            'C5_3'=>$respuestas['C5_3'],
                            'C5_3_1'=>$respuestas['C5_3.1'],
                            'C5_3_1_1'=>$respuestas['C5_3.1.1'],
                            'C5_3_2'=>$respuestas['C5_3.2'],
                            'C5_3_2_1'=>$respuestas['C5_3.2.1'],
                            'C5_3_3'=>$respuestas['C5_3.3'],
                            'C5_3_3_1'=>$respuestas['C5_3.3.1'],
                            'C5_4'=>$respuestas['C5_4'],
                            'C5_5'=>$respuestas['C5_5'],
                            'C5_6'=>$respuestas['C5_6'],
                            'C5_6_1'=>$respuestas['C5_6.1'],
                            'C5_6_2'=>$respuestas['C5_6.2'],
                            'C5_7'=>$respuestas['C5_7'],
                            'C5_7_1_A'=>$respuestas['C5_7.1']['C5_7.1_A'],
                            'C5_7_1_B'=>$respuestas['C5_7.1']['C5_7.1_B'],
                            'C5_7_1_C'=>$respuestas['C5_7.1']['C5_7.1_C'],
                            'C5_7_1_D'=>$respuestas['C5_7.1']['C5_7.1_D'],
                            'C5_7_1_E'=>$respuestas['C5_7.1']['C5_7.1_E'],
                            'C5_7_2'=>$respuestas['C5_7.2'],
                            'C5_8'=>$respuestas['C5_8'],
                            'C5_8_1'=>$respuestas['C5_8.1'],
                            'C5_9'=>$respuestas['C5_9'],
                            'C5_9_1'=>$respuestas['C5_9.1'],
                            'C5_9_2'=>$respuestas['C5_9.2'],
                            'C5_10'=>$respuestas['C5_10'],
                            'C5_10_1'=>$respuestas['C5_10.1'],
                            'C5_11'=>$respuestas['C5_11'],
                            'C5_12'=>$respuestas['C5_12'],
                            'C5_12_1'=>$respuestas['C5_12.1'],
                            'C5_13'=>$respuestas['C5_13'],
                            'C5_13_1'=>$respuestas['C5_13.1']['medicamento']['C5_13.1'],
                            'C5_13_1_1'=>$respuestas['C5_13.1']['uso']['C5_13.1'],
                            'C5_13_1_2'=>$respuestas['C5_13.1']['receto']['C5_13.1'],
                            'C5_13_2'=>$respuestas['C5_13.1']['medicamento']['C5_13.2'],
                            'C5_13_2_1'=>$respuestas['C5_13.1']['uso']['C5_13.2'],
                            'C5_13_2_2'=>$respuestas['C5_13.1']['receto']['C5_13.2'],
                            'C5_13_3'=>$respuestas['C5_13.1']['medicamento']['C5_13.3'],
                            'C5_13_3_1'=>$respuestas['C5_13.1']['uso']['C5_13.3'],
                            'C5_13_3_2'=>$respuestas['C5_13.1']['receto']['C5_13.3'],
                            'C5_13_4'=>$respuestas['C5_13.1']['medicamento']['C5_13.4'],
                            'C5_13_4_1'=>$respuestas['C5_13.1']['uso']['C5_13.4'],
                            'C5_13_4_2'=>$respuestas['C5_13.1']['receto']['C5_13.4'],
                            'C5_14'=>$respuestas['C5_14'],
                            'C5_14_1'=>$respuestas['C5_14.1'],
                            'C5_14_2'=>$respuestas['C5_14.2'],
                            'C5_15'=>$respuestas['C5_15'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                        $c5->save();

                    if (!empty($examenLab)) 
                        {
                            foreach ($examenLab as $key => $value) {
                                $examenLab = ExamenLaboratorio::create([
                                    'cod_preg_encuesta' => $key,
                                    'examen' => $value,
                                    'idpersona' => $id_persona,
                                    'cuestionario' => $n_question
                                ]);
                                $examenLab->save();
                            }
                        }

                        if (!empty($positivo == 'Si')) 
                        {
                            $reincide = ReincidenciaCovidUser::create([
                                'recuerda_fecha' => $fecha_aprox_prueba,
                                'fecha_prueba' => $fecha_prueba,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question,
                                'dosis' => '1'
                            ]);
                            $reincide->save();
                        }

                        if (!empty($consulta == 'Si')) 
                        {
                            $prof = ConsultaProfUser::create([
                                'medio' => $medio,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question,
                                'dosis' => '1'
                            ]);
                            $prof->save();
                        }

                        if (!empty($medicamento) && !empty($uso)) 
                        {
                                $cantMedi = count($medicamento);
                                $cantUso = count($uso);
                                if($cantMedi == $cantUso)
                                {
                                    $cantidad = $cantMedi;
                                    for ($i=1; $i <= $cantidad; $i++) 
                                    { 
                                        $medicina = MedicamentoPostVacuna::create([
                                            'idpersona' => $id_persona,
                                            'medicamento' => $medicamento['C5_13.'.$i],
                                            'uso' => $uso['C5_13.'.$i],
                                            'recomendado_por' => $recetado_por['C5_13.'.$i],
                                            'cuestionario' => $n_question
                                        ]);
                                    }
                                    $medicina->save();
                                }
                        }

                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }else{
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }

        }catch(\Exception $e) {            
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }
    }
}