<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\C4b1Bot;
use App\Models\ConsultaProfUser;
use Illuminate\Http\Request;
use App\Models\CuestionarioPorUsuario;
use App\Models\ExamenLaboratorio;
use App\Models\ReincidenciaCovidUser;
use App\Models\SintomasC1b;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class C4b1BotController extends Controller{

    public function store(Request $request)
    {
        try{ 
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);

            $examenLab = Resources::ExamenLab($respuestas['C4B1_9.1']);
            $sintomas= Resources::Sintomas($respuestas['C4B1_5_A']);
            $positivo =$respuestas['C4B1_14'];
            $fecha_aprox_prueba = $respuestas['C4B1_14.1'];
            isset($respuestas['C4B1_14.2'])?$fecha_prueba=$respuestas['C4B1_14.2']:$fecha_prueba='';
            $consulta = $respuestas['C4B1_15'];
            $consulta == 'Si'?$medio= Resources::Medio(Resources::QuitaAcento($respuestas['C4B1_15.1'])):$medio='';

            $sintoma=$sintomas[0]['sintoma'];
            $fecha_inicio=$sintomas[0]['fecha_inicio'];
            $fecha_fin=$sintomas[0]['fecha_fin'];
            $mejoro=$sintomas[0]['mejoro'];
            $intensidad=$sintomas[0]['intensidad'];
            

            if ($id_persona != 0) 
            {
                DB::beginTransaction();
                try {
                        $C4b1 = C4b1Bot::create([
                            'C4B1_3'=>$respuestas['C4B1_3'],
                            'C4B1_4'=>$respuestas['C4B1_4'],
                            'C4B1_4_1'=>$respuestas['C4B1_4.1'],
                            'C4B1_4_2'=>$respuestas['C4B1_4.2'],
                            'C4B1_5'=>$respuestas['C4B1_5'],
                            'C4B1_5_1_1'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.1'],
                            'C4B1_5_2_1'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.1'],
                            'C4B1_5_3_1'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.1'],
                            'C4B1_5_4_1'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.1'],
                            'C4B1_5_5_1'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.1'],
                            'C4B1_5_1_2'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.2'],
                            'C4B1_5_2_2'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.2'],
                            'C4B1_5_3_2'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.2'],
                            'C4B1_5_4_2'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.2'],
                            'C4B1_5_5_2'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.2'],
                            'C4B1_5_1_3'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.3'],
                            'C4B1_5_2_3'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.3'],
                            'C4B1_5_3_3'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.3'],
                            'C4B1_5_4_3'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.3'],
                            'C4B1_5_5_3'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.3'],
                            'C4B1_5_1_4'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.4'],
                            'C4B1_5_2_4'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.4'],
                            'C4B1_5_3_4'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.4'],
                            'C4B1_5_4_4'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.4'],
                            'C4B1_5_5_4'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.4'],
                            'C4B1_5_1_5'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.5'],
                            'C4B1_5_2_5'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.5'],
                            'C4B1_5_3_5'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.5'],
                            'C4B1_5_4_5'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.5'],
                            'C4B1_5_5_5'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.5'],
                            'C4B1_5_1_6'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.6'],
                            'C4B1_5_2_6'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.6'],
                            'C4B1_5_3_6'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.6'],
                            'C4B1_5_4_6'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.6'],
                            'C4B1_5_5_6'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.6'],
                            'C4B1_5_1_7'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.7'],
                            'C4B1_5_2_7'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.7'],
                            'C4B1_5_3_7'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.7'],
                            'C4B1_5_4_7'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.7'],
                            'C4B1_5_5_7'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.7'],
                            'C4B1_5_1_8'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.8'],
                            'C4B1_5_2_8'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.8'],
                            'C4B1_5_3_8'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.8'],
                            'C4B1_5_4_8'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.8'],
                            'C4B1_5_5_8'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.8'],
                            'C4B1_5_1_9'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.9'],
                            'C4B1_5_2_9'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.9'],
                            'C4B1_5_3_9'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.9'],
                            'C4B1_5_4_9'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.9'],
                            'C4B1_5_5_9'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.9'],
                            'C4B1_5_1_10'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.10'],
                            'C4B1_5_2_10'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.10'],
                            'C4B1_5_3_10'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.10'],
                            'C4B1_5_4_10'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.10'],
                            'C4B1_5_5_10'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.10'],
                            'C4B1_5_1_11'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.11'],
                            'C4B1_5_2_11'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.11'],
                            'C4B1_5_3_11'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.11'],
                            'C4B1_5_4_11'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.11'],
                            'C4B1_5_5_11'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.11'],
                            'C4B1_5_1_12'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.12'],
                            'C4B1_5_2_12'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.12'],
                            'C4B1_5_3_12'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.12'],
                            'C4B1_5_4_12'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.12'],
                            'C4B1_5_5_12'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.12'],
                            'C4B1_5_1_13'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.13'],
                            'C4B1_5_2_13'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.13'],
                            'C4B1_5_3_13'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.13'],
                            'C4B1_5_4_13'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.13'],
                            'C4B1_5_5_13'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.13'],
                            'C4B1_5_1_14'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.14'],
                            'C4B1_5_2_14'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.14'],
                            'C4B1_5_3_14'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.14'],
                            'C4B1_5_4_14'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.14'],
                            'C4B1_5_5_14'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.14'],
                            'C4B1_5_1_15'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.15'],
                            'C4B1_5_2_15'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.15'],
                            'C4B1_5_3_15'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.15'],
                            'C4B1_5_4_15'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.15'],
                            'C4B1_5_5_15'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.15'],
                            'C4B1_5_1_16'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.16'],
                            'C4B1_5_2_16'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.16'],
                            'C4B1_5_3_16'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.16'],
                            'C4B1_5_4_16'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.16'],
                            'C4B1_5_5_16'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.16'],
                            'C4B1_5_1_17'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.17'],
                            'C4B1_5_2_17'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.17'],
                            'C4B1_5_3_17'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.17'],
                            'C4B1_5_4_17'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.17'],
                            'C4B1_5_5_17'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.17'],
                            'C4B1_5_1_18'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.18'],
                            'C4B1_5_2_18'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.18'],
                            'C4B1_5_3_18'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.18'],
                            'C4B1_5_4_18'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.18'],
                            'C4B1_5_5_18'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.18'],
                            'C4B1_5_1_19'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.19'],
                            'C4B1_5_2_19'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.19'],
                            'C4B1_5_3_19'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.19'],
                            'C4B1_5_4_19'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.19'],
                            'C4B1_5_5_19'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.19'],
                            'C4B1_5_1_20'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.20'],
                            'C4B1_5_2_20'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.20'],
                            'C4B1_5_3_20'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.20'],
                            'C4B1_5_4_20'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.20'],
                            'C4B1_5_5_20'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.20'],
                            'C4B1_5_1_21'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.21'],
                            'C4B1_5_2_21'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.21'],
                            'C4B1_5_3_21'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.21'],
                            'C4B1_5_4_21'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.21'],
                            'C4B1_5_5_21'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.21'],
                            'C4B1_5_1_22'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.22'],
                            'C4B1_5_2_22'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.22'],
                            'C4B1_5_3_22'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.22'],
                            'C4B1_5_4_22'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.22'],
                            'C4B1_5_5_22'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.22'],
                            'C4B1_5_1_23'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.23'],
                            'C4B1_5_2_23'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.23'],
                            'C4B1_5_3_23'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.23'],
                            'C4B1_5_4_23'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.23'],
                            'C4B1_5_5_23'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.23'],
                            'C4B1_5_1_24'=>$respuestas['C4B1_5_A']['sintoma']['C4B1_5.1.24'],
                            'C4B1_5_2_24'=>$respuestas['C4B1_5_A']['fecha_inicio']['C4B1_5.2.24'],
                            'C4B1_5_3_24'=>$respuestas['C4B1_5_A']['fecha_fin']['C4B1_5.3.24'],
                            'C4B1_5_4_24'=>$respuestas['C4B1_5_A']['mejoro']['C4B1_5.4.24'],
                            'C4B1_5_5_24'=>$respuestas['C4B1_5_A']['intensidad']['C4B1_5.5.24'],
                            'C4B1_6'=>$respuestas['C4B1_6'],
                            'C4B1_6_1'=>$respuestas['C4B1_6.1'],
                            'C4B1_6_2'=>$respuestas['C4B1_6.2'],
                            'C4B1_7'=>$respuestas['C4B1_7'],
                            'C4B1_8'=>$respuestas['C4B1_8'],
                            'C4B1_8_1'=>$respuestas['C4B1_8.1'],
                            'C4B1_8_2'=>$respuestas['C4B1_8.2'],
                            'C4B1_9'=>$respuestas['C4B1_9'],
                            'C4B1_9_1_A'=>$respuestas['C4B1_9.1']['C4B1_9.1_A'],
                            'C4B1_9_1_B'=>$respuestas['C4B1_9.1']['C4B1_9.1_B'],
                            'C4B1_9_1_C'=>$respuestas['C4B1_9.1']['C4B1_9.1_C'],
                            'C4B1_9_1_D'=>$respuestas['C4B1_9.1']['C4B1_9.1_D'],
                            'C4B1_9_1_E'=>$respuestas['C4B1_9.1']['C4B1_9.1_E'],
                            'C4B1_9_2'=>$respuestas['C4B1_9.2'],
                            'C4B1_10'=>$respuestas['C4B1_10'],
                            'C4B1_10_1'=>$respuestas['C4B1_10.1'],
                            'C4B1_10_2'=>$respuestas['C4B1_10.2'],
                            'C4B1_11'=>$respuestas['C4B1_11'],
                            'C4B1_11_1'=>$respuestas['C4B1_11.1'],
                            'C4B1_12'=>$respuestas['C4B1_12'],
                            'C4B1_12_1'=>$respuestas['C4B1_12.1'],
                            'C4B1_13'=>$respuestas['C4B1_13'],
                            'C4B1_14'=>$respuestas['C4B1_14'],
                            'C4B1_14_1'=>$respuestas['C4B1_14.1'],
                            'C4B1_14_2'=>$respuestas['C4B1_14.2'],
                            'C4B1_15'=>$respuestas['C4B1_15'],
                            'C4B1_15_1'=>$respuestas['C4B1_15.1'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                        $C4b1->save();

                    if (!empty($examenLab)) 
                    {
                        foreach ($examenLab as $key => $value) {
                            $examenLab = ExamenLaboratorio::create([
                                'cod_preg_encuesta' => $key,
                                'examen' => $value,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question
                            ]);
                            $examenLab->save();
                        }
                    }

                    if (!empty($positivo == 'Si')) 
                    {
                        $reincide = ReincidenciaCovidUser::create([
                            'recuerda_fecha' => $fecha_aprox_prueba,
                            'fecha_prueba' => $fecha_prueba,
                            'idpersona' => $id_persona,
                            'cuestionario' => $n_question,
                            'dosis' => '1'
                        ]);
                        $reincide->save();
                    }

                    if (!empty($consulta == 'Si')) 
                    {
                        $prof = ConsultaProfUser::create([
                            'medio' => $medio,
                            'idpersona' => $id_persona,
                            'cuestionario' => $n_question,
                            'dosis' => '1'
                        ]);
                        $prof->save();
                    }

                    if (!empty($sintoma) && !empty($fecha_inicio) && !empty($mejoro) && !empty($intensidad)) 
                    {
                            $cantidadSintoma = count($sintoma);

                            for ($i=1; $i <= $cantidadSintoma; $i++) 
                            { 
                                $medicina = SintomasC1b::create([
                                    'idpersona' => $id_persona,
                                    'sintoma' => $sintoma['C4B1_5.1.'.$i],
                                    'fecha_inicio' => $fecha_inicio['C4B1_5.2.'.$i],
                                    'fecha_fin' => $fecha_fin['C4B1_5.3.'.$i],
                                    'mejoro' => $mejoro['C4B1_5.4.'.$i],
                                    'intensidad' => $intensidad['C4B1_5.5.'.$i]
                                ]);
                            }
                            $medicina->save();
                    }

                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }
            else
            {
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }

        }catch(\Exception $e) {            
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }

    }

}