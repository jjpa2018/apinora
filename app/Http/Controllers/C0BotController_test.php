<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\C0Bot;
use App\Models\CieTutorBot;
use App\Models\CuestionarioPorUsuario;
use App\Models\Persona;
use App\Models\ReactogeneidadUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class C0BotController extends Controller
{ 
    public function store(Request $request)
    {
        try 
        {
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $telefono = $request->input('telefono');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $respuestas = array_filter($respuestas);
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $existe = Resources::ConsultaExiste($respuestas['C0.1.1']);

            if ($existe == 0) 
            {
                if(!empty($n_question) && !empty($respuestas))
                {
                    //if ($edad >= 18 && $respuestas['C0.1'] === 'Yo') {
                    DB::beginTransaction();
                        try {
                            //$grupo = Resources::defineGrupoReacto($grupo, $edad, $telefono, $fecha);

                            $persona = Persona::create([
                                'passport' => $respuestas['C0.1'],
                                'cedula_informante' => $respuestas['C0.1.1'],
                                'cod_unico' => uniqid(),
                                'nombre_participante' => $respuestas['C0.2'],
                                'fecha_nacimiento' => $respuestas['C0.3'],
                                'edad' => null,
                                'telefono' => $telefono,
                                'conector_pau' => $telefono,
                                'correo_informante' => '',
                                'grupo' => null,
                            ]);
                            $persona->save();
                            
                            $id_persona = Resources::ConsultaExiste($respuestas['C0.1.1']);

                            /*if ($grupo == 1 || $grupo == 2 || $grupo == 3) {
                                $grupo_reacto_user = ReactogeneidadUser::create([
                                    'telefono' => $telefono,
                                    'grupo' => $grupo,
                                    'fecha' => $fecha,
                                    'estatus' => '1',
                                ]);
                                $grupo_reacto_user->save();
                            }*/

                            /*if($respuestas['C0.1'] != 'Yo')
                            {
                                $tutor = CieTutorBot::create([
                                    'cedula_informante'    => $respuestas['C0.1.3'],
                                    'nombre_representante' => $respuestas['C0.1.2'],
                                    'fecha_nacimiento_r'   => $respuestas['C0.1.4'],
                                    'prentesco'            => $respuestas['C0.1.1'],
                                    'telefono'             => $telefono
                                ]);
                                $tutor->save();
                                unset($respuestas['C0.1.1'],$respuestas['C0.1.2'],$respuestas['C0.1.3'],$respuestas['C0.1.4']);
                            }

                            foreach ($respuestas as $key => $value) {
                                $c0 = C0Bot::create([
                                    'cod_preg_encuesta' => $key,
                                    'respuesta' => $value,
                                    'fecha_operacion' => $fecha,
                                    'hora' => $hora,
                                    'telefono' => $telefono,
                                ]);
                                $c0->save();
                            }*/

                            $pregunta_user = CuestionarioPorUsuario::create([
                                'cuestionario' => $n_question,
                                'idpersona' => $id_persona,
                                'fecha' => $fecha,
                                'hora' => $hora,
                                'estatus' => '1',
                            ]);
                            $pregunta_user->save();
                         DB::commit();
                            return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                        } catch (\Throwable $th) {
                            DB::rollback();
                            return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                        }
                    /*}
                    else
                    {
                        return response()->json(['errorCode' => 800, 'msj' => 'El usuario es menor de edad, debe ser asistido por un adulto']);
                    }*/
                }
                else
                {
                    return response()->json(['errorCode' => 600, 'msj' => 'Faltan parametros para ejecutar esta acción']);
                }
            }
            else
            {
                return response()->json(['errorCode' => 300, 'msj' => 'Ya existe un registro con estos datos']);
            }
            
        } catch (\Exception $e) {
            return response()->json(['errorCode' => 500, 'errorMessage' => 'Error en la ejecucion del servicio'], 500);
        }
    }

}
