<?php
namespace App\Http\Controllers;

use App\Models\CuestionarioPorUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenericosNoraController extends Controller{

    public function consultaEstadoSujeto(Request $request)
    {
        //try {
            $conector_pau = $request->input('conector_id');
            DB::statement("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));");
            $respuesta= DB::table('persona as p')
                            ->select('p.cod_unico as numCaso',
                                    'p.nombre_participante as nombre',
                                    'p.grupo as grupo',
                                    'd.dosis as dosis',
                                    DB::raw('MAX(cu.cuestionario) as cuestionario')
                                    )
                            ->join('cuestionario_por_usuario as cu','cu.idpersona','=','p.id')
                            ->join('c0-bot as c','c.idpersona','=','p.id')
                            ->join('dosisusuario as d','d.idpersona','=','p.id')
                            ->where('p.conector_pau',$conector_pau)
                            ->groupBy('cod_unico')
                            ->get();
            return $respuesta;dd();
            $prueba = $this->procesaDatos($respuesta);
            return $prueba;
            
            
        /*} catch (\Throwable $e) {
            return response()->json(['errorCode' => 500, 'errorMessage' => 'Error en la ejecucion del servicio'], 500);
        }*/
        

        
    }

    public function procesaDatos($respuesta)
    {
        $contarRegistro= count($respuesta);
        $c2=[];
        $c3=[];
        $dosis=[];
            for ($i=0; $i < $contarRegistro; $i++) 
            { 
                $dosis_c[] = $respuesta[$i]->dosis;
                $encuesta[] = $this->contarCuestionarios($respuesta[$i]->numCaso);
                $c2 = $encuesta[$i]['c2'];
                $c3 = $encuesta[$i]['c3'];

                if(($dosis_c == 1 && $c2 == 1 && $c3 == 1)||($dosis_c == 1 && $c2 == 1))
                {
                    $dosis = 1;
                }
                if(($dosis_c == 2 && $c2 == 1 && $c3 == 2)||($dosis_c == 2 && $c3 == 3))
                {
                    $dosis = 2;
                }
                if(($dosis_c == 3 && $c2 == 1 && $c3 == 3)||($dosis_c == 1 && $c3 == 3))
                {
                    $dosis = 3;
                } 
                //unset($respuesta[$i]->cuestionario);
            }
            
        //return $encuesta[0]['c2']; 
    }

    public function saberDosis()
    {
        # code...
    }

    public function contarCuestionarios($numCaso)
    {
        $id_persona = DB::table('persona')
                    ->select('id')
                    ->where('cod_unico',$numCaso)
                    ->where('estatus','1')
                    ->get();
        
        $c2 = CuestionarioPorUsuario::where('cuestionario','like', '%c2')
                                    ->where('idpersona',$id_persona[0]->id)
                                    ->where('estatus','1')
                                    ->get();
        $cantidadc2 = count($c2); 

        $c3 = CuestionarioPorUsuario::where('cuestionario','like', '%c3')
                                    ->where('idpersona',$id_persona[0]->id)
                                    ->where('estatus','1')
                                    ->get();
        $cantidadc3 = count($c3); 

        $respuesta = array('c2'=>$cantidadc2,'c3'=>$cantidadc3);
        
        return $respuesta;
        
    }

    public function fechaSeguimiento($grupo,$cuestionario)
    {
        //consulto la fecha que le corresponde el seguimiento
        //verifico si es hoy
        //cosulto la fecha en que se vacuno en el c3 c3.2
        //segun la fecha en que se vacuno, la dosis que tiene, el grupo al que pertenece le doy fecha proxima para cuestionario
        //
    }

}