<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\C0Bot;
use App\Models\CieTutorBot;
use App\Models\CuestionarioPorUsuario;
use App\Models\Persona;
use App\Models\ReactogeneidadUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class C0BotController extends Controller
{ 
    public function store(Request $request)
    {
        try 
        {
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $telefono = $request->input('telefono');
            $conector = $request->input('conector_pau');
            $grupo = $request->input('grupo');
            $edad = $request->input('edad');
            $dias_vacuna = $request->input('dias');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $respuestas['C0.1'] == 'Yo'?$responde = 1:$responde = 2;

            if(!empty($respuestas['C0.5.1']))
            {
                $doc_identifica = $respuestas['C0.5.1'];
            }
            else
            {
                $doc_identifica = $respuestas['C0.5.2'];
            }

            if ($respuestas['C0.2'] == 'No') {
                $estatus = 0;
            }
            else{
                $estatus = 1;
            }

            $existe = Resources::ConsultaExiste($doc_identifica);
           
            if ($existe == 0) 
            {
                if ($grupo != 'E') {
                    $grupo = Resources::defineGrupoReacto($grupo, $edad, $telefono, $fecha);
                }else{
                    $grupo = 100;
                    $estatus = 2;
                }

                if(!empty($n_question) && !empty($edad) && !empty($respuestas))
                {
                    if (($edad >= 17 && $responde == 1) || ($edad <= 16 && $responde == 2)) {
                        DB::beginTransaction();
                        try {
                            
                            $persona = Persona::create([
                                'tiene_pasaporte' => $respuestas['C0.5'],
                                'doc_identifica' => $doc_identifica,
                                'cod_unico' => uniqid(),
                                'nombre_participante' => $respuestas['C0.6'],
                                'fecha_nacimiento' => $respuestas['C0.7'],
                                'edad' => $edad,
                                'telefono' => $telefono,
                                'conector_pau' => $conector,
                                'correo_informante' => $respuestas['C0.8.1'],
                                'grupo' => $grupo,
                                'estatus' => $estatus
                            ]);
                            $persona->save();
                            $id_persona = Resources::ConsultaExiste($doc_identifica);

                            if ($grupo == 1 || $grupo == 2 || $grupo == 3) {
                                $grupo_reacto_user = ReactogeneidadUser::create([
                                    'idpersona' => $id_persona,
                                    'grupo' => $grupo,
                                    'fecha' => $fecha,
                                    'estatus' => $estatus,
                                ]);
                                $grupo_reacto_user->save();
                            }

                            if($responde != '1')
                            {
                                $tutor = CieTutorBot::create([
                                    'cedula_panama'        => $respuestas['C0.1.3'],
                                    'cedula_informante'    => $respuestas['C0.1.3.1'],
                                    'nombre_representante' => $respuestas['C0.1.2'],
                                    'fecha_nacimiento_r'   => $respuestas['C0.1.4'],
                                    'prentesco'            => $respuestas['C0.1.1'],
                                    'idpersona'            => $id_persona,
                                    'telefono'             => $telefono
                                ]);
                                $tutor->save();
                            }
                                $c0 = C0Bot::create([
                                    'C0_1' => $respuestas['C0.1'],
                                    'C0_1_1' => $respuestas['C0.1.1'],
                                    'C0_1_2' => $respuestas['C0.1.2'],
                                    'C0_1_3' => $respuestas['C0.1.3'],
                                    'C0_1_3_1' => $respuestas['C0.1.3.1'],
                                    'C0_1_4' => $respuestas['C0.1.4'],
                                    'C0_2' => $respuestas['C0.2'],
                                    'C0_2_1' => $respuestas['C0.2.1'],
                                    'C0_2_2' => $dias_vacuna,
                                    'C0_3' => $respuestas['C0.3'],
                                    'C0_4' => $respuestas['C0.4'],
                                    'C0_5' => $respuestas['C0.5'],
                                    'C0_5_1' => $respuestas['C0.5.1'],
                                    'C0_5_2' => $respuestas['C0.5.2'],
                                    'C0_6' => $respuestas['C0.6'],
                                    'C0_7' => $respuestas['C0.7'],
                                    'C0_8' => $respuestas['C0.8'],
                                    'C0_8_1' => $respuestas['C0.8.1'],
                                    'idpersona' => $id_persona,
                                    'fecha_operacion' => $fecha,
                                    'hora' => $hora,
                                ]);
                                $c0->save();

                            $pregunta_user = CuestionarioPorUsuario::create([
                                'cuestionario' => $n_question,
                                'idpersona' => $id_persona,
                                'fecha' => $fecha,
                                'hora' => $hora,
                                'estatus' => '1',
                            ]);
                            $pregunta_user->save();
                            $numCaso = Resources::ConsultaCodigo($id_persona);
                            DB::commit();
                            return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito','numeroCaso'=>$numCaso]);
                        } catch (\Throwable $th) {
                            DB::rollback();
                            return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                        }
                    }
                    else
                    {
                        return response()->json(['errorCode' => 800, 'msj' => 'Verifique la información del participante']);
                    }
                }
                else
                {
                    return response()->json(['errorCode' => 600, 'msj' => 'Faltan parametros para ejecutar esta acción']);
                }
            }
            else
            {
                return response()->json(['errorCode' => 300, 'msj' => 'Ya existe un registro con estos datos']);
            }
            
        } catch (\Exception $e) {
            return response()->json(['errorCode' => 500, 'errorMessage' => 'Error en la ejecucion del servicio'], 500);
        }
    }

}
