<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\C0Bot;
use App\Models\C1Bot;
use App\Models\C2Bot;
use App\Models\C3Bot;
use App\Models\C4a1Bot;
use App\Models\C4a2Bot;
use App\Models\C4b1Bot;
use App\Models\C4b2Bot;
use App\Models\C4b3Bot;
use App\Models\C5Bot;
use App\Models\Cr1bBot;
use App\Models\Cr1Bot;
use App\Models\Cr2bBot;
use App\Models\Cr2Bot;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CuadroMandoController extends Controller
{
    public function genero()
    {
        $generos =  DB::select('SELECT
                                (SELECT COUNT(C1_6) FROM `c1-bot`	WHERE C1_6 like "%Masculino%") as masculino,
                                (SELECT COUNT(C1_6) FROM `c1-bot`	WHERE C1_6 like "%Femenino%") as femenino
                                FROM `c1-bot`
                                GROUP BY masculino , femenino');  
        if (!empty($generos)) {
            return response()->json([
                                    ['label' => 'Masculino','value' => $generos[0]->masculino],
                                    ['label' => 'Femenino','value' => $generos[0]->femenino]
                                ]);
        }else{
            return response()->json([
                                    ['label' => 'Masculino','value' => '0'],
                                    ['label' => 'Femenino','value' => '0']
                                ]);
        }
        
    }

    public function grupoEdad()
    {
        $grupos =  DB::select('SELECT
                                (SELECT COUNT(grupo) FROM persona WHERE grupo = "1") as grupo_1,
                                (SELECT COUNT(grupo) FROM persona WHERE grupo = "11") as grupo_11,
                                (SELECT COUNT(grupo) FROM persona WHERE grupo = "2") as grupo_2,
                                (SELECT COUNT(grupo) FROM persona WHERE grupo = "12") as grupo_12,
                                (SELECT COUNT(grupo) FROM persona WHERE grupo = "3") as grupo_3,
                                (SELECT COUNT(grupo) FROM persona WHERE grupo = "13") as grupo_13
                            FROM persona
                            GROUP BY grupo_1,grupo_11,grupo_2,grupo_12,grupo_3,grupo_13');
        if (!empty($grupos)) 
        {
            $grupo_5_12 = ($grupos[0]->grupo_1) + ($grupos[0]->grupo_11);
            $grupo_13_16 = ($grupos[0]->grupo_2) + ($grupos[0]->grupo_12);
            $grupo_17 = ($grupos[0]->grupo_3) + ($grupos[0]->grupo_13);

            return response()->json([
                                    ['label'=>'Grupo 5 - 12 años','value'=>$grupo_5_12],
                                    ['label'=>'Grupo 13 - 16 años','value'=>$grupo_13_16],
                                    ['label'=>'Grupo > 17 años','value'=>$grupo_17],
                                ]);
        }else
        {
            return response()->json([
                ['label'=>'Grupo 5 - 12 años','value'=>'0'],
                ['label'=>'Grupo 13 - 16 años','value'=>'0'],
                ['label'=>'Grupo > 17 años','value'=>'0']
            ]);
        }
        
    }

    public function demografico()
    {
        $demografia =  DB::select('SELECT
                                    (SELECT count(id) as total FROM direccion_paciente) as total,
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "1") as "PA_1",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "2") as "PA_2",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "3") as "PA_3",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "4") as "PA_4",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "5") as "PA_5",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "6") as "PA_6",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "7") as "PA_7",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "8") as "PA_8",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "9") as "PA_9",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "10") as "PA_KY",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "11") as "PA_EM",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "12") as "PA_NB",
                                    (SELECT CONCAT(ROUND((COUNT(provincia)/ total * 100))) FROM direccion_paciente WHERE provincia = "13") as "PA_10"
                                FROM direccion_paciente
                                GROUP BY "PA-1","PA-2","PA-3","PA-4","PA-5","PA-6","PA-7","PA-8","PA-9","PA-KY","PA-EM","PA-NB","PA-10"');
        if (!empty($demografia)) {
            return response()->json([
                                    ['id'=>'PA-1','value'=>$demografia[0]->PA_1],
                                    ['id'=>'PA-2','value'=>$demografia[0]->PA_2],
                                    ['id'=>'PA-3','value'=>$demografia[0]->PA_3],
                                    ['id'=>'PA-4','value'=>$demografia[0]->PA_4],
                                    ['id'=>'PA-5','value'=>$demografia[0]->PA_5],
                                    ['id'=>'PA-6','value'=>$demografia[0]->PA_6],
                                    ['id'=>'PA-7','value'=>$demografia[0]->PA_7],
                                    ['id'=>'PA-8','value'=>$demografia[0]->PA_8],
                                    ['id'=>'PA-9','value'=>$demografia[0]->PA_9],
                                    ['id'=>'PA-KY','value'=>$demografia[0]->PA_KY],
                                    ['id'=>'PA-EM','value'=>$demografia[0]->PA_EM],
                                    ['id'=>'PA-NB','value'=>$demografia[0]->PA_NB],
                                    ['id'=>'PA-10','value'=>$demografia[0]->PA_10]
                                ]);
        }else{
            return response()->json([
                                    ['id'=>'PA-1','value'=>'0'],
                                    ['id'=>'PA-2','value'=>'0'],
                                    ['id'=>'PA-3','value'=>'0'],
                                    ['id'=>'PA-4','value'=>'0'],
                                    ['id'=>'PA-5','value'=>'0'],
                                    ['id'=>'PA-6','value'=>'0'],
                                    ['id'=>'PA-7','value'=>'0'],
                                    ['id'=>'PA-8','value'=>'0'],
                                    ['id'=>'PA-9','value'=>'0'],
                                    ['id'=>'PA-KY','value'=>'0'],
                                    ['id'=>'PA-EM','value'=>'0'],
                                    ['id'=>'PA-NB','value'=>'0'],
                                    ['id'=>'PA-10','value'=>'0']
                                ]);
        }
    }

    public function indicadores()
    {
        $indicadores =  DB::select('SELECT
                                (SELECT COUNT(id) FROM persona) as "Universo",
                                (SELECT COUNT(id) FROM persona WHERE estatus <> "2") as "Activo",
                                (SELECT COUNT(id) FROM persona WHERE estatus = "2") as "Excluido"
                                FROM persona
                                GROUP BY Universo,Activo,Excluido');
        if (!empty($indicadores)) {
            return response()->json([
                                    ['label'=>'Universo','value'=>$indicadores[0]->Universo],
                                    ['label'=>'Activos','value'=>$indicadores[0]->Activo],
                                    ['label'=>'Excluidos','value'=>$indicadores[0]->Excluido]
                                ]);
        }else{
            return response()->json([
                                    ['label'=>'Universo','value'=>'0'],
                                    ['label'=>'Activos','value'=>'0'],
                                    ['label'=>'Excluidos','value'=>'0']
                                ]);
        }
    }

    public function EndpointImage(Request $request)
    {
        $file = $request->input('url_name');
        $url_base = $request->root()."/imagen/".$file;
        system("mv ".public_path($file)." ".storage_path('app/public/'));
        return $url_base;

    }

    public function dataCuestionarios()
    {
        $dataCuestionarios =  DB::select('SELECT
                                (SELECT COUNT(id) FROM `c0-tutor`) as "C0_tutor",
                                (SELECT CONCAT(ROUND((COUNT(id)-C0_tutor))) FROM cuestionario_por_usuario WHERE cuestionario = "C0") as "C0",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "C1") as "C1",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "C2") as "C2",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "C3") as "C3",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "C4a1") as "C4a1",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "C4a2") as "C4a2",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "C4b1") as "C4b1",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "C4B2") as "C4b2",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "C4b3") as "C4b3",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "cr1") as "R1A",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "CR1B") as "R1B",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "CR2A") as "R2A",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "CR2B") as "R2B",
                                (SELECT COUNT(id) FROM cuestionario_por_usuario WHERE cuestionario = "C5") as "C5"
                                FROM cuestionario_por_usuario
                                GROUP BY C0,C1,C2,C3,C4a1,C4a2,C4b1,C4b2,C4b3,R1A,R1B,R2A,R2B,C5');
        if (!empty($dataCuestionarios)) {
            return response()->json([
                                    ['label'=>'C0_tutor','value'=>$dataCuestionarios[0]->C0_tutor],
                                    ['label'=>'C0','value'=>$dataCuestionarios[0]->C0],
                                    ['label'=>'C1','value'=>$dataCuestionarios[0]->C1],
                                    ['label'=>'C2','value'=>$dataCuestionarios[0]->C2],
                                    ['label'=>'C3','value'=>$dataCuestionarios[0]->C3],
                                    ['label'=>'C4a1','value'=>$dataCuestionarios[0]->C4a1],
                                    ['label'=>'C4a2','value'=>$dataCuestionarios[0]->C4a2],
                                    ['label'=>'C4b1','value'=>$dataCuestionarios[0]->C4b1],
                                    ['label'=>'C4b2','value'=>$dataCuestionarios[0]->C4b2],
                                    ['label'=>'C4b3','value'=>$dataCuestionarios[0]->C4b3],
                                    ['label'=>'R1A','value'=>$dataCuestionarios[0]->R1A],
                                    ['label'=>'R1B','value'=>$dataCuestionarios[0]->R1B],
                                    ['label'=>'R2A','value'=>$dataCuestionarios[0]->R2A],
                                    ['label'=>'R2B','value'=>$dataCuestionarios[0]->R2B],
                                    ['label'=>'C5','value'=>$dataCuestionarios[0]->C5]
                                ]);
        }else{
            $prueba = array([
                                    ['label'=>'C0','value'=>'0'],
                                    ['label'=>'C1','value'=>'0'],
                                    ['label'=>'C2','value'=>'0'],
                                    ['label'=>'C3','value'=>'0'],
                                    ['label'=>'C4a1','value'=>'0'],
                                    ['label'=>'C4a2','value'=>'0'],
                                    ['label'=>'C4b1','value'=>'0'],
                                    ['label'=>'C4b2','value'=>'0'],
                                    ['label'=>'C4b3','value'=>'0'],
                                    ['label'=>'R1A','value'=>'0'],
                                    ['label'=>'R1B','value'=>'0'],
                                    ['label'=>'R2A','value'=>'0'],
                                    ['label'=>'R2B','value'=>'0'],
                                    ['label'=>'C5','value'=>'0']
                                ]);
                                return $prueba;
        }
    }

    public function cuestionariosExport(Request $request)
    {
        $cuestionario = $request->input('cuestionario');
        
        switch ($cuestionario) {
            case 'C0':
                    $query = DB::table('c0-bot')
                            ->select('C0_1','C0_1_1','C0_1_2','C0_1_3','C0_1_3_1','C0_1_4','C0_2','C0_2_1','C0_2_2','C0_3','C0_4','C0_5','C0_5_1','C0_5_2','C0_6','C0_7','C0_8','C0_8_1')
                            ->get();
                    return json_encode($query);
                break;

            case 'C1':
                    $query = DB::table('c1-bot')
                            ->select('C1_1','C1_1_1','C1_3','C1_3_1','C1_3_2','C1_3_3','C1_4','C1_4_1','C1_5','C1_5_1','C1_5_2','C1_5_3','C1_5_4','C1_6','C1_7','C1_8','C1_9','C1_9_1','C1_10','C1_10_1','C1_11','C1_11_1_A','C1_11_1_B','C1_11_1_C','C1_11_1_D','C1_11_1_E','C1_11_1_F','C1_11_1_G','C1_11_1_H','C1_11_1_I','C1_11_1_J','C1_11_1_K','C1_11_1_L','C1_12','C1_13','C1_14','C1_15','C1_17','C1_18','C1_19','C1_20','C1_21','C1_21_1','C1_22','C1_22_1','C1_23','C1_23_1','C1_23_2','C1_24','C1_24_1_1','C1_24_1_1_1','C1_24_1_2','C1_24_1_2_1','C1_24_1_3','C1_24_1_3_1','C1_24_1_4','C1_24_1_4_1','C1_25','C1_25_1','C1_25_2','C1_25_3','C1_25_4','C1_25_5')
                            ->get();
                    return json_encode($query);
                break;
            case 'C2':
                    $query = DB::table('c2-bot')
                            ->select('C2_1','C2_2','C2_2_3','C2_5','C2_5_1','C2_6','C2_6_1','C2_7','C2_7_1','C2_8','C2_8_1','C2_9','C2_9_1','C2_10','C2_10_1','C2_11','C2_11_1','C2_12','C2_12_1','C2_12_2')
                            ->get();
                    return json_encode($query);
                break;
            case 'C3':
                    $query = DB::table('c3-bot')
                            ->select('C3_1','C3_2','C3_3','C3_3_1','C3_4','C3_5','C3_5_1','C3_6','C3_6_1','C3_7','C3_8','C3_9','C3_9_1','C3_9_2')
                            ->get();
                    return json_encode($query);
                break;
            case 'C4A1':
                    $query = DB::table('c4a1-bot')
                            ->select('C4A1_3','C4A1_3_1','C4A1_3_2','C4A1_4','C4A1_4_1_1','C4A1_4_2_1','C4A1_4_3_1','C4A1_4_4_1','C4A1_4_5_1','C4A1_4_1_2','C4A1_4_2_2','C4A1_4_3_2','C4A1_4_4_2','C4A1_4_5_2','C4A1_4_1_3','C4A1_4_2_3','C4A1_4_3_3','C4A1_4_4_3','C4A1_4_5_3','C4A1_4_1_4','C4A1_4_2_4','C4A1_4_3_4','C4A1_4_4_4','C4A1_4_5_4','C4A1_4_1_5','C4A1_4_2_5','C4A1_4_3_5','C4A1_4_4_5','C4A1_4_5_5','C4A1_4_1_6','C4A1_4_2_6','C4A1_4_3_6','C4A1_4_4_6','C4A1_4_5_6','C4A1_4_1_7','C4A1_4_2_7','C4A1_4_3_7','C4A1_4_4_7','C4A1_4_5_7','C4A1_4_1_8','C4A1_4_2_8','C4A1_4_3_8','C4A1_4_4_8','C4A1_4_5_8','C4A1_4_1_9','C4A1_4_2_9','C4A1_4_3_9','C4A1_4_4_9','C4A1_4_5_9','C4A1_4_1_10','C4A1_4_2_10','C4A1_4_3_10','C4A1_4_4_10','C4A1_4_5_10','C4A1_4_1_11','C4A1_4_2_11','C4A1_4_3_11','C4A1_4_4_11','C4A1_4_5_11','C4A1_4_1_12','C4A1_4_2_12','C4A1_4_3_12','C4A1_4_4_12','C4A1_4_5_12','C4A1_4_1_13','C4A1_4_2_13','C4A1_4_3_13','C4A1_4_4_13','C4A1_4_5_13','C4A1_4_1_14','C4A1_4_2_14','C4A1_4_3_14','C4A1_4_4_14','C4A1_4_5_14','C4A1_4_1_15','C4A1_4_2_15','C4A1_4_3_15','C4A1_4_4_15','C4A1_4_5_15','C4A1_4_1_16','C4A1_4_2_16','C4A1_4_3_16','C4A1_4_4_16','C4A1_4_5_16','C4A1_4_1_17','C4A1_4_2_17','C4A1_4_3_17','C4A1_4_4_17','C4A1_4_5_17','C4A1_4_1_18','C4A1_4_2_18','C4A1_4_3_18','C4A1_4_4_18','C4A1_4_5_18','C4A1_4_1_19','C4A1_4_2_19','C4A1_4_3_19','C4A1_4_4_19','C4A1_4_5_19','C4A1_4_1_20','C4A1_4_2_20','C4A1_4_3_20','C4A1_4_4_20','C4A1_4_5_20','C4A1_4_1_21','C4A1_4_2_21','C4A1_4_3_21','C4A1_4_4_21','C4A1_4_5_21','C4A1_4_1_22','C4A1_4_2_22','C4A1_4_3_22','C4A1_4_4_22','C4A1_4_5_22','C4A1_4_1_23','C4A1_4_2_23','C4A1_4_3_23','C4A1_4_4_23','C4A1_4_5_23','C4A1_4_1_24','C4A1_4_2_24','C4A1_4_3_24','C4A1_4_4_24','C4A1_4_5_24','C4A1_5','C4A1_5_1','C4A1_5_2','C4A1_6','C4A1_7','C4A1_7_1','C4A1_7_2','C4A1_8','C4A1_8_1_A','C4A1_8_1_B','C4A1_8_1_C','C4A1_8_1_D','C4A1_8_1_E','C4A1_8_1','C4A1_8_2','C4A1_9','C4A1_9_1','C4A1_9_2','C4A1_10','C4A1_10_1','C4A1_11','C4A1_11_1','C4A1_12','C4A1_12_1','C4A1_13','C4A1_13_1','C4A1_13_2','C4A1_14','C4A1_14_1')
                            ->get();
                    return json_encode($query);
                break;
            case 'C4A2':
                    $query = DB::table('c4a2-bot')
                            ->select('C4A2_1','C4A2_2','C4A2_3','C4A2_3_1','C4A2_3_1_1','C4A2_3_2','C4A2_3_2_1','C4A2_3_3','C4A2_3_3_1','C4A2_4','C4A2_5','C4A2_6','C4A2_6_1','C4A2_6_2','C4A2_7','C4A2_7_1_A','C4A2_7_1_B','C4A2_7_1_C','C4A2_7_1_D','C4A2_7_1_E','C4A2_7_2','C4A2_8','C4A2_8_1','C4A2_9','C4A2_9_1','C4A2_9_2','C4A2_10','C4A2_10_1','C4A2_11','C4A2_12','C4A2_12_1','C4A2_13','C4A2_13_1','C4A2_13_1_1','C4A2_13_1_2','C4A2_13_2','C4A2_13_2_1','C4A2_13_2_2','C4A2_13_3','C4A2_13_3_1','C4A2_13_3_2','C4A2_13_4','C4A2_13_4_1','C4A2_13_4_2','C4A2_14','C4A2_14_1','C4A2_14_2','C4A2_15')
                            ->get();
                    return json_encode($query);
                break;
            case 'CR1A':
                    $query = DB::table('cr1-bot')
                            ->select('CR1_3','CR1_3_1','CR1_3_2','CR1_4','CR1_4_1_1','CR1_4_2_1','CR1_4_3_1','CR1_4_4_1','CR1_4_5_1','CR1_4_1_2','CR1_4_2_2','CR1_4_3_2','CR1_4_4_2','CR1_4_5_2','CR1_4_1_3','CR1_4_2_3','CR1_4_3_3','CR1_4_4_3','CR1_4_5_3','CR1_4_1_4','CR1_4_2_4','CR1_4_3_4','CR1_4_4_4','CR1_4_5_4','CR1_4_1_5','CR1_4_2_5','CR1_4_3_5','CR1_4_4_5','CR1_4_5_5','CR1_4_1_6','CR1_4_2_6','CR1_4_3_6','CR1_4_4_6','CR1_4_5_6','CR1_4_1_7','CR1_4_2_7','CR1_4_3_7','CR1_4_4_7','CR1_4_5_7','CR1_4_1_8','CR1_4_2_8','CR1_4_3_8','CR1_4_4_8','CR1_4_5_8','CR1_4_1_9','CR1_4_2_9','CR1_4_3_9','CR1_4_4_9','CR1_4_5_9','CR1_4_1_10','CR1_4_2_10','CR1_4_3_10','CR1_4_4_10','CR1_4_5_10','CR1_4_1_11','CR1_4_2_11','CR1_4_3_11','CR1_4_4_11','CR1_4_5_11','CR1_4_1_12','CR1_4_2_12','CR1_4_3_12','CR1_4_4_12','CR1_4_5_12','CR1_4_1_13','CR1_4_2_13','CR1_4_3_13','CR1_4_4_13','CR1_4_5_13','CR1_4_1_14','CR1_4_2_14','CR1_4_3_14','CR1_4_4_14','CR1_4_5_14','CR1_4_1_15','CR1_4_2_15','CR1_4_3_15','CR1_4_4_15','CR1_4_5_15','CR1_4_1_16','CR1_4_2_16','CR1_4_3_16','CR1_4_4_16','CR1_4_5_16','CR1_4_1_17','CR1_4_2_17','CR1_4_3_17','CR1_4_4_17','CR1_4_5_17','CR1_4_1_18','CR1_4_2_18','CR1_4_3_18','CR1_4_4_18','CR1_4_5_18','CR1_4_1_19','CR1_4_2_19','CR1_4_3_19','CR1_4_4_19','CR1_4_5_19','CR1_4_1_20','CR1_4_2_20','CR1_4_3_20','CR1_4_4_20','CR1_4_5_20','CR1_4_1_21','CR1_4_2_21','CR1_4_3_21','CR1_4_4_21','CR1_4_5_21','CR1_4_1_22','CR1_4_2_22','CR1_4_3_22','CR1_4_4_22','CR1_4_5_22','CR1_4_1_23','CR1_4_2_23','CR1_4_3_23','CR1_4_4_23','CR1_4_5_23','CR1_4_1_24','CR1_4_2_24','CR1_4_3_24','CR1_4_4_24','CR1_4_5_24','CR1_5','CR1_5_1','CR1_5_2','CR1_6','CR1_7','CR1_7_1','CR1_7_2','CR1_8','CR1_8_1_A','CR1_8_1_B','CR1_8_1_C','CR1_8_1_D','CR1_8_1_E','CR1_8_2','CR1_9','CR1_9_1','CR1_9_2','CR1_10','CR1_10_1','CR1_11','CR1_11_1','CR1_12','CR1_12_1','CR1_13','CR1_13_1','CR1_13_2','CR1_14','CR1_14_1')
                            ->get();
                    return json_encode($query);
                break;
            case 'CR1B':
                    $query = DB::table('cr1b-bot')
                            ->select('CR1B_1','CR1B_2','CR1B_3','CR1B_3_1','CR1B_3_1_1','CR1B_3_2','CR1B_3_2_1','CR1B_3_3','CR1B_3_3_1','CR1B_4','CR1B_5','CR1B_6','CR1B_6_1','CR1B_6_2','CR1B_7','CR1B_7_1','CR1B_8','CR1B_8_1_A','CR1B_8_1_B','CR1B_8_1_C','CR1B_8_1_D','CR1B_8_1_E','CR1B_8_2','CR1B_9','CR1B_9_1','CR1B_10','CR1B_11','CR1B_11_1','CR1B_11_2','CR1B_12','CR1B_12_1','CR1B_13','CR1B_13_1','CR1B_13_2','CR1B_14','CR1B_14_1')
                            ->get();
                    return json_encode($query);
                break;
            case 'CR2A':
                    $query = DB::table('cr2-bot')
                            ->select('CR2_3','CR2_4','CR2_4_1','CR2_4_2','CR2_5','CR2_5_1_1','CR2_5_2_1','CR2_5_3_1','CR2_5_4_1','CR2_5_5_1','CR2_5_1_2','CR2_5_2_2','CR2_5_3_2','CR2_5_4_2','CR2_5_5_2','CR2_5_1_3','CR2_5_2_3','CR2_5_3_3','CR2_5_4_3','CR2_5_5_3','CR2_5_1_4','CR2_5_2_4','CR2_5_3_4','CR2_5_4_4','CR2_5_5_4','CR2_5_1_5','CR2_5_2_5','CR2_5_3_5','CR2_5_4_5','CR2_5_5_5','CR2_5_1_6','CR2_5_2_6','CR2_5_3_6','CR2_5_4_6','CR2_5_5_6','CR2_5_1_7','CR2_5_2_7','CR2_5_3_7','CR2_5_4_7','CR2_5_5_7','CR2_5_1_8','CR2_5_2_8','CR2_5_3_8','CR2_5_4_8','CR2_5_5_8','CR2_5_1_9','CR2_5_2_9','CR2_5_3_9','CR2_5_4_9','CR2_5_5_9','CR2_5_1_10','CR2_5_2_10','CR2_5_3_10','CR2_5_4_10','CR2_5_5_10','CR2_5_1_11','CR2_5_2_11','CR2_5_3_11','CR2_5_4_11','CR2_5_5_11','CR2_5_1_12','CR2_5_2_12','CR2_5_3_12','CR2_5_4_12','CR2_5_5_12','CR2_5_1_13','CR2_5_2_13','CR2_5_3_13','CR2_5_4_13','CR2_5_5_13','CR2_5_1_14','CR2_5_2_14','CR2_5_3_14','CR2_5_4_14','CR2_5_5_14','CR2_5_1_15','CR2_5_2_15','CR2_5_3_15','CR2_5_4_15','CR2_5_5_15','CR2_5_1_16','CR2_5_2_16','CR2_5_3_16','CR2_5_4_16','CR2_5_5_16','CR2_5_1_17','CR2_5_2_17','CR2_5_3_17','CR2_5_4_17','CR2_5_5_17','CR2_5_1_18','CR2_5_2_18','CR2_5_3_18','CR2_5_4_18','CR2_5_5_18','CR2_5_1_19','CR2_5_2_19','CR2_5_3_19','CR2_5_4_19','CR2_5_5_19','CR2_5_1_20','CR2_5_2_20','CR2_5_3_20','CR2_5_4_20','CR2_5_5_20','CR2_5_1_21','CR2_5_2_21','CR2_5_3_21','CR2_5_4_21','CR2_5_5_21','CR2_5_1_22','CR2_5_2_22','CR2_5_3_22','CR2_5_4_22','CR2_5_5_22','CR2_5_1_23','CR2_5_2_23','CR2_5_3_23','CR2_5_4_23','CR2_5_5_23','CR2_5_1_24','CR2_5_2_24','CR2_5_3_24','CR2_5_4_24','CR2_5_5_24','CR2_6','CR2_6_1','CR2_6_2','CR2_7','CR2_8','CR2_8_1','CR2_8_2','CR2_9','CR2_9_1_A','CR2_9_1_B','CR2_9_1_C','CR2_9_1_D','CR2_9_1_E','CR2_9_2','CR2_10','CR2_10_1','CR2_10_2','CR2_11','CR2_11_1','CR2_12','CR2_13','CR2_13_1','CR2_13_2','CR2_14','CR2_14_1')
                            ->get();
                    return json_encode($query);
                break;
            case 'CR2B':
                    $query = DB::table('cr2b-bot')
                            ->select('CR2B_1','CR2B_2','CR2B_3','CR2B_3_1','CR2B_3_1_1','CR2B_3_2','CR2B_3_2_1','CR2B_3_3','CR2B_3_3_1','CR2B_4','CR2B_5','CR2B_6','CR2B_6_1','CR2B_6_2','CR2B_7','CR2B_7_1','CR2B_8','CR2B_8_1_A','CR2B_8_1_B','CR2B_8_1_C','CR2B_8_1_D','CR2B_8_1_E','CR2B_8_2','CR2B_9','CR2B_9_1','CR2B_10','CR2B_11','CR2B_11_1','CR2B_11_2','CR2B_12','CR2B_12_1','CR2B_13','CR2B_13_1','CR2B_13_2','CR2B_14','CR2B_14_1')
                            ->get();
                    return json_encode($query);
                break;
            case 'C4B1':
                    $query = DB::table('c4b1-bot')
                            ->select('C4B1_3','C4B1_4','C4B1_4_1','C4B1_4_2','C4B1_5','C4B1_5_1_1','C4B1_5_2_1','C4B1_5_3_1','C4B1_5_4_1','C4B1_5_5_1','C4B1_5_1_2','C4B1_5_2_2','C4B1_5_3_2','C4B1_5_4_2','C4B1_5_5_2','C4B1_5_1_3','C4B1_5_2_3','C4B1_5_3_3','C4B1_5_4_3','C4B1_5_5_3','C4B1_5_1_4','C4B1_5_2_4','C4B1_5_3_4','C4B1_5_4_4','C4B1_5_5_4','C4B1_5_1_5','C4B1_5_2_5','C4B1_5_3_5','C4B1_5_4_5','C4B1_5_5_5','C4B1_5_1_6','C4B1_5_2_6','C4B1_5_3_6','C4B1_5_4_6','C4B1_5_5_6','C4B1_5_1_7','C4B1_5_2_7','C4B1_5_3_7','C4B1_5_4_7','C4B1_5_5_7','C4B1_5_1_8','C4B1_5_2_8','C4B1_5_3_8','C4B1_5_4_8','C4B1_5_5_8','C4B1_5_1_9','C4B1_5_2_9','C4B1_5_3_9','C4B1_5_4_9','C4B1_5_5_9','C4B1_5_1_10','C4B1_5_2_10','C4B1_5_3_10','C4B1_5_4_10','C4B1_5_5_10','C4B1_5_1_11','C4B1_5_2_11','C4B1_5_3_11','C4B1_5_4_11','C4B1_5_5_11','C4B1_5_1_12','C4B1_5_2_12','C4B1_5_3_12','C4B1_5_4_12','C4B1_5_5_12','C4B1_5_1_13','C4B1_5_2_13','C4B1_5_3_13','C4B1_5_4_13','C4B1_5_5_13','C4B1_5_1_14','C4B1_5_2_14','C4B1_5_3_14','C4B1_5_4_14','C4B1_5_5_14','C4B1_5_1_15','C4B1_5_2_15','C4B1_5_3_15','C4B1_5_4_15','C4B1_5_5_15','C4B1_5_1_16','C4B1_5_2_16','C4B1_5_3_16','C4B1_5_4_16','C4B1_5_5_16','C4B1_5_1_17','C4B1_5_2_17','C4B1_5_3_17','C4B1_5_4_17','C4B1_5_5_17','C4B1_5_1_18','C4B1_5_2_18','C4B1_5_3_18','C4B1_5_4_18','C4B1_5_5_18','C4B1_5_1_19','C4B1_5_2_19','C4B1_5_3_19','C4B1_5_4_19','C4B1_5_5_19','C4B1_5_1_20','C4B1_5_2_20','C4B1_5_3_20','C4B1_5_4_20','C4B1_5_5_20','C4B1_5_1_21','C4B1_5_2_21','C4B1_5_3_21','C4B1_5_4_21','C4B1_5_5_21','C4B1_5_1_22','C4B1_5_2_22','C4B1_5_3_22','C4B1_5_4_22','C4B1_5_5_22','C4B1_5_1_23','C4B1_5_2_23','C4B1_5_3_23','C4B1_5_4_23','C4B1_5_5_23','C4B1_5_1_24','C4B1_5_2_24','C4B1_5_3_24','C4B1_5_4_24','C4B1_5_5_24','C4B1_6','C4B1_6_1','C4B1_6_2','C4B1_7','C4B1_8','C4B1_8_1','C4B1_8_2','C4B1_9','C4B1_9_1_A','C4B1_9_1_B','C4B1_9_1_C','C4B1_9_1_D','C4B1_9_1_E','C4B1_9_2','C4B1_10','C4B1_10_1','C4B1_10_2','C4B1_11','C4B1_11_1','C4B1_12','C4B1_12_1','C4B1_13','C4B1_14','C4B1_14_1','C4B1_14_2','C4B1_15','C4B1_15_1')
                            ->get();
                    return json_encode($query);
                break;
            case 'C4B2':
                    $query = DB::table('c4b2-bot')
                            ->select('C4B2_1','C4B2_2','C4B2_3','C4B2_4','C4B2_5','C4B2_6','C4B2_6_1','C4B2_6_2','C4B2_7','C4B2_7_1_A','C4B2_7_1_B','C4B2_7_1_C','C4B2_7_1_D','C4B2_7_1_E','C4B2_7_2','C4B2_8','C4B2_8_1','C4B2_9','C4B2_9_1','C4B2_9_2','C4B2_10','C4B2_10_1','C4B2_11','C4B2_12','C4B2_12_1','C4B2_13','C4B2_13_1','C4B2_13_1_1','C4B2_13_1_2','C4B2_13_2','C4B2_13_2_1','C4B2_13_2_2','C4B2_13_3','C4B2_13_3_1','C4B2_13_3_2','C4B2_13_4','C4B2_13_4_1','C4B2_13_4_2','C4B2_14','C4B2_14_1','C4B2_14_2','C4B2_15')
                            ->get();
                    return json_encode($query);
                break;
            case 'C4B3':
                    $query = DB::table('c4b3-bot')
                            ->select('C4B3_1','C4B3_2','C4B3_3','C4B3_3_1','C4B3_3_2','C4B3_4','C4B3_4_1_A','C4B3_4_1_B','C4B3_4_1_C','C4B3_4_1_D','C4B3_4_1_E','C4B3_4_2','C4B3_5','C4B3_5_1','C4B3_6','C4B3_6_1','C4B3_6_2','C4B3_7','C4B3_7_1','C4B3_8','C4B3_9','C4B3_9_1','C4B3_10','C4B3_10_1','C4B3_10_1_1','C4B3_10_1_2','C4B3_10_2','C4B3_10_2_1','C4B3_10_2_2','C4B3_10_3','C4B3_10_3_1','C4B3_10_3_2','C4B3_10_4','C4B3_10_4_1','C4B3_10_4_2','C4B3_11','C4B3_11_1','C4B3_11_2','C4B3_12')
                            ->get();
                    return json_encode($query);
                break;
            case 'C5':
                    $query = DB::table('c5-bot')
                            ->select('C5_1','C5_2','C5_3','C5_3_1','C5_3_1_1','C5_3_2','C5_3_2_1','C5_3_3','C5_3_3_1','C5_4','C5_5','C5_6','C5_6_1','C5_6_2','C5_7','C5_7_1_A','C5_7_1_B','C5_7_1_C','C5_7_1_D','C5_7_1_E','C5_7_1','C5_7_2','C5_8','C5_8_1','C5_9','C5_9_1','C5_9_2','C5_10','C5_10_1','C5_11','C5_12','C5_12_1','C5_13','C5_13_1','C5_13_1_1','C5_13_1_2','C5_13_2','C5_13_2_1','C5_13_2_2','C5_13_3','C5_13_3_1','C5_13_3_2','C5_13_4','C5_13_4_1','C5_13_4_2','C5_14','C5_14_1','C5_14_2','C5_15')
                            ->get();
                    return json_encode($query);
                break;          
            
            default:
                return response()->json(['code'=>'400','mes'=>'No hay datos']);
                break;
        }
    }

    public function downloadImage($id)
    {
        $file = Storage::disk('public')->get($id);
  
        return (new Response($file, 200))
              ->header('Content-Type', 'image/jpeg');
    
    }
}








