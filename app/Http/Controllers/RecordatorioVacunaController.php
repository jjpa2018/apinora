<?php
namespace App\Http\Controllers;

use App\Models\C0Bot;
use App\Models\Persona;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RecordatorioVacunaController extends Controller
{

	public function actionDaily()
	{
		try {
			$c='c0-bot';
			$p='persona';
			$telefonosNotificar = DB::table($c)
								  ->join('persona',$p.'.telefono','=',$c.'.telefono')
								  ->select(''.$c.'.respuesta',''.$c.'.telefono')
								  ->where($c.'.cod_preg_encuesta','=','C0.3')
								  ->where($p.'.estatus', '=', 0)
								  ->get();
			$count = count($telefonosNotificar);			
			for ($i=0; $i < $count ; $i++) { 
				$fecha[]=  $telefonosNotificar[$i]->respuesta;
				$telefono[]=  $telefonosNotificar[$i]->telefono;
			}
			return response()->json(['fecha'=>$fecha,'telefono'=>$telefono]);
		} catch (\Throwable $th) {
			//throw $th;
		}
	}
}