<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\Cr2Bot;
use Illuminate\Http\Request;
use App\Models\CuestionarioPorUsuario;
use App\Models\ExamenLaboratorio;
use App\Models\SintomasR2a;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class Cr2BotController extends Controller{

	public function store(Request $request)
    {
        try{
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);

            if(!empty(array_filter($respuestas['CR2_9.1'])))
            {
                $examenLab = array_filter($respuestas['CR2_9.1']);
            } 
            else
            {
                $examenLab['CR2_9.1']= '';
            }
            
            if (!empty(array_filter($respuestas['CR2_5_A']))) {
                $sintoma = array_filter($respuestas['CR2_5_A']['sintoma']);           
                $fecha_inicio = array_filter($respuestas['CR2_5_A']['fecha_inicio']); 
                $fecha_fin = array_filter($respuestas['CR2_5_A']['fecha_fin']);           
                $mejoro = array_filter($respuestas['CR2_5_A']['mejoro']); 
                $intensidad = array_filter($respuestas['CR2_5_A']['intensidad']); 
            }
            else{
                $sintoma= '';
                $fecha_inicio= '';
                $fecha_fin= '';
                $mejoro= '';
                $intensidad = '';
            }

            if ($id_persona != 0) 
            {
                DB::beginTransaction();
                try {
                        $cr2 = Cr2Bot::create([
                            'CR2_3'=>$respuestas['CR2_3'],
                            'CR2_4'=>$respuestas['CR2_4'],
                            'CR2_4_1'=>$respuestas['CR2_4.1'],
                            'CR2_4_2'=>$respuestas['CR2_4.2'],
                            'CR2_5'=>$respuestas['CR2_5'],
                            'CR2_5_1_1'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.1'],
                            'CR2_5_2_1'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.1'],
                            'CR2_5_3_1'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.1'],
                            'CR2_5_4_1'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.1'],
                            'CR2_5_5_1'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.1'],
                            'CR2_5_1_2'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.2'],
                            'CR2_5_2_2'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.2'],
                            'CR2_5_3_2'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.2'],
                            'CR2_5_4_2'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.2'],
                            'CR2_5_5_2'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.2'],
                            'CR2_5_1_3'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.3'],
                            'CR2_5_2_3'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.3'],
                            'CR2_5_3_3'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.3'],
                            'CR2_5_4_3'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.3'],
                            'CR2_5_5_3'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.3'],
                            'CR2_5_1_4'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.4'],
                            'CR2_5_2_4'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.4'],
                            'CR2_5_3_4'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.4'],
                            'CR2_5_4_4'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.4'],
                            'CR2_5_5_4'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.4'],
                            'CR2_5_1_5'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.5'],
                            'CR2_5_2_5'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.5'],
                            'CR2_5_3_5'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.5'],
                            'CR2_5_4_5'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.5'],
                            'CR2_5_5_5'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.5'],
                            'CR2_5_1_6'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.6'],
                            'CR2_5_2_6'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.6'],
                            'CR2_5_3_6'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.6'],
                            'CR2_5_4_6'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.6'],
                            'CR2_5_5_6'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.6'],
                            'CR2_5_1_7'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.7'],
                            'CR2_5_2_7'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.7'],
                            'CR2_5_3_7'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.7'],
                            'CR2_5_4_7'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.7'],
                            'CR2_5_5_7'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.7'],
                            'CR2_5_1_8'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.8'],
                            'CR2_5_2_8'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.8'],
                            'CR2_5_3_8'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.8'],
                            'CR2_5_4_8'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.8'],
                            'CR2_5_5_8'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.8'],
                            'CR2_5_1_9'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.9'],
                            'CR2_5_2_9'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.9'],
                            'CR2_5_3_9'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.9'],
                            'CR2_5_4_9'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.9'],
                            'CR2_5_5_9'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.9'],
                            'CR2_5_1_10'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.10'],
                            'CR2_5_2_10'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.10'],
                            'CR2_5_3_10'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.10'],
                            'CR2_5_4_10'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.10'],
                            'CR2_5_5_10'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.10'],
                            'CR2_5_1_11'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.11'],
                            'CR2_5_2_11'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.11'],
                            'CR2_5_3_11'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.11'],
                            'CR2_5_4_11'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.11'],
                            'CR2_5_5_11'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.11'],
                            'CR2_5_1_12'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.12'],
                            'CR2_5_2_12'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.12'],
                            'CR2_5_3_12'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.12'],
                            'CR2_5_4_12'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.12'],
                            'CR2_5_5_12'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.12'],
                            'CR2_5_1_13'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.13'],
                            'CR2_5_2_13'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.13'],
                            'CR2_5_3_13'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.13'],
                            'CR2_5_4_13'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.13'],
                            'CR2_5_5_13'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.13'],
                            'CR2_5_1_14'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.14'],
                            'CR2_5_2_14'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.14'],
                            'CR2_5_3_14'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.14'],
                            'CR2_5_4_14'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.14'],
                            'CR2_5_5_14'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.14'],
                            'CR2_5_1_15'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.15'],
                            'CR2_5_2_15'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.15'],
                            'CR2_5_3_15'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.15'],
                            'CR2_5_4_15'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.15'],
                            'CR2_5_5_15'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.15'],
                            'CR2_5_1_16'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.16'],
                            'CR2_5_2_16'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.16'],
                            'CR2_5_3_16'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.16'],
                            'CR2_5_4_16'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.16'],
                            'CR2_5_5_16'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.16'],
                            'CR2_5_1_17'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.17'],
                            'CR2_5_2_17'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.17'],
                            'CR2_5_3_17'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.17'],
                            'CR2_5_4_17'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.17'],
                            'CR2_5_5_17'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.17'],
                            'CR2_5_1_18'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.18'],
                            'CR2_5_2_18'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.18'],
                            'CR2_5_3_18'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.18'],
                            'CR2_5_4_18'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.18'],
                            'CR2_5_5_18'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.18'],
                            'CR2_5_1_19'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.19'],
                            'CR2_5_2_19'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.19'],
                            'CR2_5_3_19'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.19'],
                            'CR2_5_4_19'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.19'],
                            'CR2_5_5_19'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.19'],
                            'CR2_5_1_20'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.20'],
                            'CR2_5_2_20'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.20'],
                            'CR2_5_3_20'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.20'],
                            'CR2_5_4_20'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.20'],
                            'CR2_5_5_20'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.20'],
                            'CR2_5_1_21'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.21'],
                            'CR2_5_2_21'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.21'],
                            'CR2_5_3_21'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.21'],
                            'CR2_5_4_21'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.21'],
                            'CR2_5_5_21'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.21'],
                            'CR2_5_1_22'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.22'],
                            'CR2_5_2_22'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.22'],
                            'CR2_5_3_22'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.22'],
                            'CR2_5_4_22'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.22'],
                            'CR2_5_5_22'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.22'],
                            'CR2_5_1_23'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.23'],
                            'CR2_5_2_23'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.23'],
                            'CR2_5_3_23'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.23'],
                            'CR2_5_4_23'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.23'],
                            'CR2_5_5_23'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.23'],
                            'CR2_5_1_24'=>$respuestas['CR2_5_A']['sintoma']['CR2_5.1.24'],
                            'CR2_5_2_24'=>$respuestas['CR2_5_A']['fecha_inicio']['CR2_5.2.24'],
                            'CR2_5_3_24'=>$respuestas['CR2_5_A']['fecha_fin']['CR2_5.3.24'],
                            'CR2_5_4_24'=>$respuestas['CR2_5_A']['mejoro']['CR2_5.4.24'],
                            'CR2_5_5_24'=>$respuestas['CR2_5_A']['intensidad']['CR2_5.5.24'],
                            'CR2_6'=>$respuestas['CR2_6'],
                            'CR2_6_1'=>$respuestas['CR2_6.1'],
                            'CR2_6_2'=>$respuestas['CR2_6.2'],
                            'CR2_7'=>$respuestas['CR2_7'],
                            'CR2_8'=>$respuestas['CR2_8'],
                            'CR2_8_1'=>$respuestas['CR2_8.1'],
                            'CR2_8_2'=>$respuestas['CR2_8.2'],
                            'CR2_9'=>$respuestas['CR2_9'],
                            'CR2_9_1_A'=>$respuestas['CR2_9.1']['CR2_9.1_A'],
                            'CR2_9_1_B'=>$respuestas['CR2_9.1']['CR2_9.1_B'],
                            'CR2_9_1_C'=>$respuestas['CR2_9.1']['CR2_9.1_C'],
                            'CR2_9_1_D'=>$respuestas['CR2_9.1']['CR2_9.1_D'],
                            'CR2_9_1_E'=>$respuestas['CR2_9.1']['CR2_9.1_E'],
                            'CR2_9_2'=>$respuestas['CR2_9.2'],
                            'CR2_10'=>$respuestas['CR2_10'],
                            'CR2_10_1'=>$respuestas['CR2_10.1'],
                            'CR2_10_2'=>$respuestas['CR2_10.2'],
                            'CR2_11'=>$respuestas['CR2_11'],
                            'CR2_11_1'=>$respuestas['CR2_11.1'],
                            'CR2_12'=>$respuestas['CR2_12'],
                            'CR2_13'=>$respuestas['CR2_13'],
                            'CR2_13_1'=>$respuestas['CR2_13.1'],
                            'CR2_13_2'=>$respuestas['CR2_13.2'],
                            'CR2_14'=>$respuestas['CR2_14'],
                            'CR2_14_1'=>$respuestas['CR2_14.1'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                        $cr2->save();

                    if (!empty($examenLab)) 
                    {
                        foreach ($examenLab as $key => $value) {
                            $examenLab = ExamenLaboratorio::create([
                                'cod_preg_encuesta' => $key,
                                'examen' => $value,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question
                            ]);
                            $examenLab->save();
                        }
                    }

                    if (!empty($sintoma) && !empty($fecha_inicio) && !empty($mejoro) && !empty($intensidad)) 
                    {
                            $cantidadSintoma = count($sintoma);

                            for ($i=1; $i <= $cantidadSintoma; $i++) 
                            { 
                                $medicina = SintomasR2a::create([
                                'idpersona' => $id_persona,
                                'sintoma' => $sintoma['CR2_5.1.'.$i],
                                'fecha_inicio' => $fecha_inicio['CR2_5.2.'.$i],
                                'fecha_fin' => $fecha_fin['CR2_5.3.'.$i],
                                'mejoro' => $mejoro['CR2_5.4.'.$i],
                                'intensidad' => $intensidad['CR2_5.5.'.$i],
                                ]);
                            }
                            $medicina->save();
                    }

                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }
            else
            {
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }

        }catch(\Exception $e) {            
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }
    }
}