<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\ConsultaProfUser;
use App\Models\Cr1bBot;
use App\Models\CuestionarioPorUsuario;
use App\Models\ExamenLaboratorio;
use App\Models\ReincidenciaCovidUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Cr1bController extends Controller{

    public function store(Request $request)
    {
        try{
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);
            $examenLab = Resources::ExamenLab($respuestas['CR1B_8.1']);

            $positivo =$respuestas['CR1B_13']; 
            $fecha_aprox_prueba = $respuestas['CR1B_13.1'];
            isset($respuestas['CR1B_13.2'])?$fecha_prueba=$respuestas['CR1B_13.2']:$fecha_prueba='';
            $consulta = $respuestas['CR1B_14'];
            $consulta == 'Si'?$medio= Resources::Medio(Resources::QuitaAcento($respuestas['CR1B_14.1'])):$medio='';

            if ($id_persona != 0)  
            {
                DB::beginTransaction();
                try {
                        $cr1b = Cr1bBot::create([
                            'CR1B_1'=>$respuestas['CR1B_1'],
                            'CR1B_2'=>$respuestas['CR1B_2'],
                            'CR1B_3'=>$respuestas['CR1B_3'],
                            'CR1B_3_1'=>$respuestas['CR1B_3.1'],
                            'CR1B_3_1_1'=>$respuestas['CR1B_3.1.1'],
                            'CR1B_3_2'=>$respuestas['CR1B_3.2'],
                            'CR1B_3_2_1'=>$respuestas['CR1B_3.2.1'],
                            'CR1B_3_3'=>$respuestas['CR1B_3.3'],
                            'CR1B_3_3_1'=>$respuestas['CR1B_3.3.1'],
                            'CR1B_4'=>$respuestas['CR1B_4'],
                            'CR1B_5'=>$respuestas['CR1B_5'],
                            'CR1B_6'=>$respuestas['CR1B_6'],
                            'CR1B_6_1'=>$respuestas['CR1B_6.1'],
                            'CR1B_6_2'=>$respuestas['CR1B_6.2'],
                            'CR1B_7'=>$respuestas['CR1B_7'],
                            'CR1B_7_1'=>$respuestas['CR1B_7.1'],
                            'CR1B_8'=>$respuestas['CR1B_8'],
                            'CR1B_8_1_A'=>$respuestas['CR1B_8.1']['CR1B_8.1_A'],
                            'CR1B_8_1_B'=>$respuestas['CR1B_8.1']['CR1B_8.1_B'],
                            'CR1B_8_1_C'=>$respuestas['CR1B_8.1']['CR1B_8.1_C'],
                            'CR1B_8_1_D'=>$respuestas['CR1B_8.1']['CR1B_8.1_D'],
                            'CR1B_8_1_E'=>$respuestas['CR1B_8.1']['CR1B_8.1_E'],
                            'CR1B_8_2'=>$respuestas['CR1B_8.2'],
                            'CR1B_9'=>$respuestas['CR1B_9'],
                            'CR1B_9_1'=>$respuestas['CR1B_9.1'],
                            'CR1B_10'=>$respuestas['CR1B_10'],
                            'CR1B_11'=>$respuestas['CR1B_11'],
                            'CR1B_11_1'=>$respuestas['CR1B_11.1'],
                            'CR1B_11_2'=>$respuestas['CR1B_11.2'],
                            'CR1B_12'=>$respuestas['CR1B_12'],
                            'CR1B_12_1'=>$respuestas['CR1B_12.1'],
                            'CR1B_13'=>$respuestas['CR1B_13'],
                            'CR1B_13_1'=>$respuestas['CR1B_13.1'],
                            'CR1B_13_2'=>$respuestas['CR1B_13.2'],
                            'CR1B_14'=>$respuestas['CR1B_14'],
                            'CR1B_14_1'=>$respuestas['CR1B_14.1'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                        $cr1b->save();

                    if (!empty($examenLab)) 
                        {
                            foreach ($examenLab as $key => $value) {
                                $examenLab = ExamenLaboratorio::create([
                                    'cod_preg_encuesta' => $key,
                                    'examen' => $value,
                                    'idpersona' => $id_persona,
                                    'cuestionario' => $n_question
                                ]);
                                $examenLab->save();
                            }
                        }

                        if (!empty($positivo == 'Si')) 
                        {
                            $reincide = ReincidenciaCovidUser::create([
                                'recuerda_fecha' => $fecha_aprox_prueba,
                                'fecha_prueba' => $fecha_prueba,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question,
                                'dosis' => '1'
                            ]);
                            $reincide->save();
                        }

                        if (!empty($consulta == 'Si')) 
                        {
                            $prof = ConsultaProfUser::create([
                                'medio' => $medio,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question,
                                'dosis' => '1'
                            ]);
                            $prof->save();
                        }

                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }else{
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }

        }catch(\Exception $e) {            
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }
    }

}