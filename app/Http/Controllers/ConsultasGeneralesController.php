<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConsultasGeneralesController extends Controller{

    public function consultaVacuna(Request $request)
    {
        $conector = $request->input('conector_pau');
        $id_persona= Resources::ConsultaSujeto($conector);

        $dosis = DB::table('c0-bot')
            ->join('persona', 'persona.id', '=', 'c0-bot.idpersona')
            ->where('c0-bot.cod_preg_encuesta','C0.8')
            ->where('c0-bot.idpersona',$id_persona)
            ->get();
        
        if ($dosis[0]->respuesta == 1) {
            return 0;
        }else {
            return 1;
        }
    }
}