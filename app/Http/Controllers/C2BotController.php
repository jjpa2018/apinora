<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\C2Bot;
use Illuminate\Http\Request;
use App\Models\CuestionarioPorUsuario;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class C2BotController extends Controller{

    public function store(Request $request)
    {
        try{
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);

            if ($id_persona != 0) 
            {
                DB::beginTransaction();
                try {
                        $c2 = C2Bot::create([
                            'C2_1'=>$respuestas['C2.1'],
                            'C2_2'=>$respuestas['C2.2'],
                            'C2_2_3'=>$respuestas['C2.2.3'],
                            'C2_5'=>$respuestas['C2.5'],
                            'C2_5_1'=>$respuestas['C2.5.1'],
                            'C2_6'=>$respuestas['C2.6'],
                            'C2_6_1'=>$respuestas['C2.6.1'],
                            'C2_7'=>$respuestas['C2.7'],
                            'C2_7_1'=>$respuestas['C2.7.1'],
                            'C2_8'=>$respuestas['C2.8'],
                            'C2_8_1'=>$respuestas['C2.8.1'],
                            'C2_9'=>$respuestas['C2.9'],
                            'C2_9_1'=>$respuestas['C2.9.1'],
                            'C2_10'=>$respuestas['C2.10'],
                            'C2_10_1'=>$respuestas['C2.10.1'],
                            'C2_11'=>$respuestas['C2.11'],
                            'C2_11_1'=>$respuestas['C2.11.1'],
                            'C2_12'=>$respuestas['C2.12'],
                            'C2_12_1'=>$respuestas['C2.12.1'],
                            'C2_12_2'=>$respuestas['C2.12.2'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                        $c2->save();


                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }
            else
            {
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }
        }catch(\Exception $e) {            
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }
    }
}