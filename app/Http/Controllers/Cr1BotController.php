<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use App\Models\ConsultaProfUser as ModelsConsultaProfUser;
use App\Models\Cr1Bot;
use Illuminate\Http\Request;
use App\Models\CuestionarioPorUsuario;
use App\Models\ExamenLaboratorio;
use App\Models\ReincidenciaCovidUser;
use App\Models\SintomasR1a;
use Carbon\Carbon;
use ConsultaProfUser;
use Illuminate\Support\Facades\DB;


class Cr1BotController extends Controller{

	public function store(Request $request)
    {
        try{
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);
            $examenLab = Resources::ExamenLab($respuestas['CR1_8.1']);
            $sintomas= Resources::Sintomas($respuestas['CR1_4_A']);
            $positivo =$respuestas['CR1_13'];
            $fecha_aprox_prueba = $respuestas['CR1_13.1'];
            isset($respuestas['CR1_13.2'])?$fecha_prueba=$respuestas['CR1_13.2']:$fecha_prueba='';
            $consulta = $respuestas['CR1_14'];
            $consulta == 'Si'?$medio= Resources::Medio(Resources::QuitaAcento($respuestas['CR1_14.1'])):$medio='';

            $sintoma=$sintomas[0]['sintoma'];
            $fecha_inicio=$sintomas[0]['fecha_inicio'];
            $fecha_fin=$sintomas[0]['fecha_fin'];
            $mejoro=$sintomas[0]['mejoro'];
            $intensidad=$sintomas[0]['intensidad'];

            if ($id_persona != 0) 
            {
                DB::beginTransaction();
                try {
                        $cr1 = Cr1Bot::create([
                            'CR1_3'=>$respuestas['CR1_3'],
                            'CR1_3_1'=>$respuestas['CR1_3.1'],
                            'CR1_3_2'=>$respuestas['CR1_3.2'],
                            'CR1_4'=>$respuestas['CR1_4'],
                            'CR1_4_1_1'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.1'],
                            'CR1_4_2_1'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.1'],
                            'CR1_4_3_1'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.1'],
                            'CR1_4_4_1'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.1'],
                            'CR1_4_5_1'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.1'],
                            'CR1_4_1_2'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.2'],
                            'CR1_4_2_2'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.2'],
                            'CR1_4_3_2'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.2'],
                            'CR1_4_4_2'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.2'],
                            'CR1_4_5_2'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.2'],
                            'CR1_4_1_3'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.3'],
                            'CR1_4_2_3'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.3'],
                            'CR1_4_3_3'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.3'],
                            'CR1_4_4_3'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.3'],
                            'CR1_4_5_3'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.3'],
                            'CR1_4_1_4'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.4'],
                            'CR1_4_2_4'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.4'],
                            'CR1_4_3_4'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.4'],
                            'CR1_4_4_4'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.4'],
                            'CR1_4_5_4'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.4'],
                            'CR1_4_1_5'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.5'],
                            'CR1_4_2_5'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.5'],
                            'CR1_4_3_5'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.5'],
                            'CR1_4_4_5'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.5'],
                            'CR1_4_5_5'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.5'],
                            'CR1_4_1_6'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.6'],
                            'CR1_4_2_6'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.6'],
                            'CR1_4_3_6'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.6'],
                            'CR1_4_4_6'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.6'],
                            'CR1_4_5_6'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.6'],
                            'CR1_4_1_7'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.7'],
                            'CR1_4_2_7'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.7'],
                            'CR1_4_3_7'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.7'],
                            'CR1_4_4_7'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.7'],
                            'CR1_4_5_7'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.7'],
                            'CR1_4_1_8'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.8'],
                            'CR1_4_2_8'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.8'],
                            'CR1_4_3_8'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.8'],
                            'CR1_4_4_8'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.8'],
                            'CR1_4_5_8'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.8'],
                            'CR1_4_1_9'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.9'],
                            'CR1_4_2_9'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.9'],
                            'CR1_4_3_9'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.9'],
                            'CR1_4_4_9'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.9'],
                            'CR1_4_5_9'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.9'],
                            'CR1_4_1_10'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.10'],
                            'CR1_4_2_10'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.10'],
                            'CR1_4_3_10'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.10'],
                            'CR1_4_4_10'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.10'],
                            'CR1_4_5_10'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.10'],
                            'CR1_4_1_11'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.11'],
                            'CR1_4_2_11'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.11'],
                            'CR1_4_3_11'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.11'],
                            'CR1_4_4_11'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.11'],
                            'CR1_4_5_11'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.11'],
                            'CR1_4_1_12'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.12'],
                            'CR1_4_2_12'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.12'],
                            'CR1_4_3_12'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.12'],
                            'CR1_4_4_12'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.12'],
                            'CR1_4_5_12'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.12'],
                            'CR1_4_1_13'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.13'],
                            'CR1_4_2_13'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.13'],
                            'CR1_4_3_13'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.13'],
                            'CR1_4_4_13'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.13'],
                            'CR1_4_5_13'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.13'],
                            'CR1_4_1_14'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.14'],
                            'CR1_4_2_14'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.14'],
                            'CR1_4_3_14'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.14'],
                            'CR1_4_4_14'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.14'],
                            'CR1_4_5_14'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.14'],
                            'CR1_4_1_15'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.15'],
                            'CR1_4_2_15'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.15'],
                            'CR1_4_3_15'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.15'],
                            'CR1_4_4_15'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.15'],
                            'CR1_4_5_15'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.15'],
                            'CR1_4_1_16'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.16'],
                            'CR1_4_2_16'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.16'],
                            'CR1_4_3_16'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.16'],
                            'CR1_4_4_16'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.16'],
                            'CR1_4_5_16'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.16'],
                            'CR1_4_1_17'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.17'],
                            'CR1_4_2_17'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.17'],
                            'CR1_4_3_17'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.17'],
                            'CR1_4_4_17'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.17'],
                            'CR1_4_5_17'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.17'],
                            'CR1_4_1_18'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.18'],
                            'CR1_4_2_18'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.18'],
                            'CR1_4_3_18'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.18'],
                            'CR1_4_4_18'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.18'],
                            'CR1_4_5_18'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.18'],
                            'CR1_4_1_19'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.19'],
                            'CR1_4_2_19'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.19'],
                            'CR1_4_3_19'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.19'],
                            'CR1_4_4_19'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.19'],
                            'CR1_4_5_19'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.19'],
                            'CR1_4_1_20'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.20'],
                            'CR1_4_2_20'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.20'],
                            'CR1_4_3_20'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.20'],
                            'CR1_4_4_20'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.20'],
                            'CR1_4_5_20'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.20'],
                            'CR1_4_1_21'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.21'],
                            'CR1_4_2_21'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.21'],
                            'CR1_4_3_21'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.21'],
                            'CR1_4_4_21'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.21'],
                            'CR1_4_5_21'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.21'],
                            'CR1_4_1_22'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.22'],
                            'CR1_4_2_22'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.22'],
                            'CR1_4_3_22'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.22'],
                            'CR1_4_4_22'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.22'],
                            'CR1_4_5_22'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.22'],
                            'CR1_4_1_23'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.23'],
                            'CR1_4_2_23'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.23'],
                            'CR1_4_3_23'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.23'],
                            'CR1_4_4_23'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.23'],
                            'CR1_4_5_23'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.23'],
                            'CR1_4_1_24'=>$respuestas['CR1_4_A']['sintoma']['CR1_4.1.24'],
                            'CR1_4_2_24'=>$respuestas['CR1_4_A']['fecha_inicio']['CR1_4.2.24'],
                            'CR1_4_3_24'=>$respuestas['CR1_4_A']['fecha_fin']['CR1_4.3.24'],
                            'CR1_4_4_24'=>$respuestas['CR1_4_A']['mejoro']['CR1_4.4.24'],
                            'CR1_4_5_24'=>$respuestas['CR1_4_A']['intensidad']['CR1_4.5.24'],
                            'CR1_5'=>$respuestas['CR1_5'],
                            'CR1_5_1'=>$respuestas['CR1_5.1'],
                            'CR1_5_2'=>$respuestas['CR1_5.2'],
                            'CR1_6'=>$respuestas['CR1_6'],
                            'CR1_7'=>$respuestas['CR1_7'],
                            'CR1_7_1'=>$respuestas['CR1_7.1'],
                            'CR1_7_2'=>$respuestas['CR1_7.2'],
                            'CR1_8'=>$respuestas['CR1_8'],
                            'CR1_8_1_A'=>$respuestas['CR1_8.1']['CR1_8.1_A'],
                            'CR1_8_1_B'=>$respuestas['CR1_8.1']['CR1_8.1_B'],
                            'CR1_8_1_C'=>$respuestas['CR1_8.1']['CR1_8.1_C'],
                            'CR1_8_1_D'=>$respuestas['CR1_8.1']['CR1_8.1_D'],
                            'CR1_8_1_E'=>$respuestas['CR1_8.1']['CR1_8.1_E'],
                            'CR1_8_2'=>$respuestas['CR1_8.2'],
                            'CR1_9'=>$respuestas['CR1_9'],
                            'CR1_9_1'=>$respuestas['CR1_9.1'],
                            'CR1_9_2'=>$respuestas['CR1_9.2'],
                            'CR1_10'=>$respuestas['CR1_10'],
                            'CR1_10_1'=>$respuestas['CR1_10.1'],
                            'CR1_11'=>$respuestas['CR1_11'],
                            'CR1_11_1'=>$respuestas['CR1_11.1'],
                            'CR1_12'=>$respuestas['CR1_12'],
                            'CR1_12_1'=>$respuestas['CR1_12.1'],
                            'CR1_13'=>$respuestas['CR1_13'],
                            'CR1_13_1'=>$respuestas['CR1_13.1'],
                            'CR1_13_2'=>$respuestas['CR1_13.2'],
                            'CR1_14'=>$respuestas['CR1_14'],
                            'CR1_14_1'=>$respuestas['CR1_14.1'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                        $cr1->save();

                    if (!empty($examenLab)) 
                    {
                        foreach ($examenLab as $key => $value) {
                            $examenLab = ExamenLaboratorio::create([
                                'cod_preg_encuesta' => $key,
                                'examen' => $value,
                                'idpersona' => $id_persona,
                                'cuestionario' => $n_question
                            ]);
                            $examenLab->save();
                        } 
                    }

                    if (!empty($positivo == 'Si')) 
                    {
                        $reincide = ReincidenciaCovidUser::create([
                            'recuerda_fecha' => $fecha_aprox_prueba,
                            'fecha_prueba' => $fecha_prueba,
                            'idpersona' => $id_persona,
                            'cuestionario' => $n_question,
                            'dosis' => '1'
                        ]);
                        $reincide->save();
                    }

                    if (!empty($consulta == 'Si')) 
                    {
                        $prof = ModelsConsultaProfUser::create([
                            'medio' => $medio,
                            'idpersona' => $id_persona,
                            'cuestionario' => $n_question,
                            'dosis' => '1'
                        ]);
                        $prof->save();
                    }

                    if (!empty($sintoma) && !empty($fecha_inicio) && !empty($mejoro) && !empty($intensidad)) 
                    {
                            $cantidadSintoma = count($sintoma);

                            for ($i=1; $i <= $cantidadSintoma; $i++) 
                            { 
                                $medicina = SintomasR1a::create([
                                'idpersona' => $id_persona,
                                'sintoma' => $sintoma['CR1_4.1.'.$i],
                                'fecha_inicio' => $fecha_inicio['CR1_4.2.'.$i],
                                'fecha_fin' => $fecha_fin['CR1_4.3.'.$i],
                                'mejoro' => $mejoro['CR1_4.4.'.$i],
                                'intensidad' => $intensidad['CR1_4.5.'.$i],
                                ]);
                            }
                            $medicina->save();
                    }

                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }
            else
            {
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }

        }catch(\Exception $e) {            
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }
    }
}