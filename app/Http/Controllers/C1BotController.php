<?php
namespace App\Http\Controllers;

use App\Extension\Resources;
use Illuminate\Http\Request;
use App\Models\C1Bot;
use App\Models\CuestionarioPorUsuario;
use App\Models\DireccionPaciente;
use App\Models\EnfermedadPaciente;
use App\Models\MedicamentoPaciente;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;



class C1BotController extends Controller{
 
    public function store(Request $request)
    {
        try{
            $now = Carbon::now();
            $n_question = $request->input('cuestionario');
            $numCaso = $request->input('numeroCaso');
            $respuestas = Resources::FormatoRespuesta($request->input('respuestas'));
            $fecha = $now->format('d-m-Y');
            $hora = $now->format('H:i');
            $id_persona = Resources::ConsultaSujeto($numCaso);

            if(!empty($respuestas['C1.11.1']))
            {
                $enfermedades = array_filter($respuestas['C1.11.1']);
            }
            else
            {
                $respuestas['C1.11.1']= '';
            } 

            if (!empty(array_filter($respuestas['C1.24.1']))) {
                $medicamento = array_filter($respuestas['C1.24.1']['medicamento']);           
                $uso = array_filter($respuestas['C1.24.1']['uso']); 
            }
            else{
                $medicamento = '';
                $uso = '';
            }
            $provincia = $respuestas['C1.5'];
            $distrito = $respuestas['C1.5.1'] ;
            $corregimiento = $respuestas['C1.5.2'];
            $poblado = $respuestas['C1.5.3'];
            $direccion = $respuestas['C1.5.4'];

            if ($id_persona != 0) 
            {
                DB::beginTransaction();
                try {                

                        $c1 = C1Bot::create([
                            'C1_1'=>$respuestas['C1.1'],
                            'C1_1_1'=>$respuestas['C1.1.1'],
                            'C1_3'=>$respuestas['C1.3'],
                            'C1_3_1'=>$respuestas['C1.3.1'],
                            'C1_3_2'=>$respuestas['C1.3.2'],
                            'C1_3_3'=>$respuestas['C1.3.3'],
                            'C1_4'=>$respuestas['C1.4'],
                            'C1_4_1'=>$respuestas['C1.4.1'],
                            'C1_5'=>$respuestas['C1.5'],
                            'C1_5_1'=>$respuestas['C1.5.1'],
                            'C1_5_2'=>$respuestas['C1.5.2'],
                            'C1_5_3'=>$respuestas['C1.5.3'],
                            'C1_5_4'=>$respuestas['C1.5.4'],
                            'C1_6'=>$respuestas['C1.6'],
                            'C1_7'=>$respuestas['C1.7'],
                            'C1_8'=>$respuestas['C1.8'],
                            'C1_9'=>$respuestas['C1.9'],
                            'C1_9_1'=>$respuestas['C1.9.1'],
                            'C1_10'=>$respuestas['C1.10'],
                            'C1_10_1'=>$respuestas['C1.10.1'],
                            'C1_11'=>$respuestas['C1.11'],
                            'C1_11_1_A'=>$respuestas['C1.11.1']['C1.11.1_A'],
                            'C1_11_1_B'=>$respuestas['C1.11.1']['C1.11.1_B'],
                            'C1_11_1_C'=>$respuestas['C1.11.1']['C1.11.1_C'],
                            'C1_11_1_D'=>$respuestas['C1.11.1']['C1.11.1_D'],
                            'C1_11_1_E'=>$respuestas['C1.11.1']['C1.11.1_E'],
                            'C1_11_1_F'=>$respuestas['C1.11.1']['C1.11.1_F'],
                            'C1_11_1_G'=>$respuestas['C1.11.1']['C1.11.1_G'],
                            'C1_11_1_H'=>$respuestas['C1.11.1']['C1.11.1_H'],
                            'C1_11_1_I'=>$respuestas['C1.11.1']['C1.11.1_I'],
                            'C1_11_1_J'=>$respuestas['C1.11.1']['C1.11.1_J'],
                            'C1_11_1_K'=>$respuestas['C1.11.1']['C1.11.1_K'],
                            'C1_11_1_L'=>$respuestas['C1.11.1']['C1.11.1_L'],
                            'C1_12'=>$respuestas['C1.12'],
                            'C1_13'=>$respuestas['C1.13'],
                            'C1_14'=>$respuestas['C1.14'],
                            'C1_15'=>$respuestas['C1.15'],
                            'C1_17'=>$respuestas['C1.17'],
                            'C1_18'=>$respuestas['C1.18'],
                            'C1_19'=>$respuestas['C1.19'],
                            'C1_20'=>$respuestas['C1.20'],
                            'C1_21'=>$respuestas['C1.21'],
                            'C1_21_1'=>$respuestas['C1.21.1'],
                            'C1_22'=>$respuestas['C1.22'],
                            'C1_22_1'=>$respuestas['C1.22.1'],
                            'C1_23'=>$respuestas['C1.23'],
                            'C1_23_1'=>$respuestas['C1.23.1'],
                            'C1_23_2'=>$respuestas['C1.23.2'],
                            'C1_24'=>$respuestas['C1.24'],
                            'C1_24_1_1'=>$respuestas['C1.24.1']['medicamento']['C1.24.1.1'],
                            'C1_24_1_1_1'=>$respuestas['C1.24.1']['uso']['C1.24.1.1'],
                            'C1_24_1_2'=>$respuestas['C1.24.1']['medicamento']['C1.24.1.2'],
                            'C1_24_1_2_1'=>$respuestas['C1.24.1']['uso']['C1.24.1.2'],
                            'C1_24_1_3'=>$respuestas['C1.24.1']['medicamento']['C1.24.1.3'],
                            'C1_24_1_3_1'=>$respuestas['C1.24.1']['uso']['C1.24.1.3'],
                            'C1_24_1_4'=>$respuestas['C1.24.1']['medicamento']['C1.24.1.4'],
                            'C1_24_1_4_1'=>$respuestas['C1.24.1']['uso']['C1.24.1.4'],
                            'C1_25'=>$respuestas['C1.25'],
                            'C1_25_1'=>$respuestas['C1.25.1'],
                            'C1_25_2'=>$respuestas['C1.25.2'],
                            'C1_25_3'=>$respuestas['C1.25.3'],
                            'C1_25_4'=>$respuestas['C1.25.4'],
                            'C1_25_5'=>$respuestas['C1.25.5'],
                            'fecha_operacion' => $fecha,
                            'hora' => $hora,
                            'idpersona' => $id_persona,
                        ]);
                       $c1->save();


                    if (!empty($enfermedades)) 
                    {
                        foreach ($enfermedades as $key => $value) {
                            $enfermedad = EnfermedadPaciente::create([
                                'cod_preg_encuesta' => $key,
                                'respuesta' => $value,
                                'fecha_operacion' => $fecha,
                                'hora' => $hora,
                                'idpersona' => $id_persona,
                            ]);
                            $enfermedad->save();
                        }
                    }

                    if (!empty($medicamento) && !empty($uso)) 
                    {
                            $cantMedi = count($medicamento);
                            $cantUso = count($uso);
                            if($cantMedi == $cantUso)
                            {
                                $cantidad = $cantMedi;
                                for ($i=1; $i <= $cantidad; $i++) 
                                { 
                                    $medicina = MedicamentoPaciente::create([
                                        'idpersona' => $id_persona,
                                        'medicamento' => $medicamento['C1.24.1.'.$i],
                                        'uso' => $uso['C1.24.1.'.$i],
                                    ]);
                                }
                                $medicina->save();
                            }
                    }
                    
                    $direccion= DireccionPaciente::create([
                        'idpersona' => $id_persona,
                        'provincia' => $provincia,
                        'distrito' => $distrito,
                        'corregimiento' => $corregimiento,
                        'poblado' => $poblado,
                        'direccion' => $direccion
                    ]);
                    $direccion->save();

                    $pregunta_user = CuestionarioPorUsuario::create([
                        'cuestionario' => $n_question,
                        'idpersona' => $id_persona,
                        'fecha' => $fecha,
                        'hora' => $hora,
                        'estatus' => '1',
                    ]);
                    $pregunta_user->save();
                    DB::commit();
                    return response()->json(['errorCode' => 200, 'msj' => 'guardado con exito']);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
                }
            }
            else
            {
                return response()->json(['errorCode' => 300, 'msj' => 'No existe un paciente registrado']);
            }
        }catch(\Exception $e) {
            return response()->json(['errorCode' => 500, 'errorMessage' => $e->getMessage()], 500);
        }
    }
}
