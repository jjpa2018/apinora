<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {   
        
        $user = $request->getUser();
        $pass = $request->getPassword();
        $condicion = $this->evaluar_condicion($user,$pass);
        if($condicion != 1)
        {
            return response('No posee permisos para ejecutar esta acción.', 401);
        }
        else
        {
            return $next($request);
        }
    }

    public function evaluar_condicion($user,$pass)
    {
        if ($user === 'nora' && $pass === '4p1n0r4') 
        {
            return 1;
        }
        elseif ($user === 'query_nora' && $pass === 'query_4p1n0r4') {
            return 1;
        }
        elseif ($user === 'mando_nora' && $pass === 'mando_4p1n0r4') {
            return 1;
        }
        elseif ($user === 'cardnet' && $pass === 'CardNet') {
            return 1;
        }
        else
        {
            return 0;
        }        
    }
}
