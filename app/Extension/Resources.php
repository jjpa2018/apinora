<?php
namespace App\Extension;

use App\Models\ReactogeneidadUser;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class Resources
{
	public static function FormatoRespuesta($respuestas)
    {
        $respuestas = str_replace('[', '', $respuestas);
        $respuestas = str_replace(']', '', $respuestas);
        $respuestas = str_replace("'", '"', $respuestas);
        $respuestas = json_decode($respuestas, true);
        return $respuestas;
    }

    public static function ConsultaExiste($doc_identifica)
    {
        $existe = DB::table('persona')->where('doc_identifica',$doc_identifica)->get();
        if(isset($existe[0]->id))
        {
            $existe = $existe[0]->id;
        }
        else
        {
            $existe = 0;
        }
        return $existe;
    }

    public static function ConsultaCodigo($id_persona)
    {
        $existe = DB::table('persona')->where('id',$id_persona)->get();
        if(isset($existe[0]->cod_unico))
        {
            $existe = $existe[0]->cod_unico;
        }
        else
        {
            $existe = 0;
        }
        return $existe;
    }

    public static function ConsultaSujeto($numCaso)
    {
        $existe = DB::table('persona')->where('cod_unico',$numCaso)->where('estatus','1')->get();
        if(isset($existe[0]->id))
        {
            $existe = $existe[0]->id;
        }
        else
        {
            $existe = 0;
        }
        return $existe;
    }

    public static function defineGrupoReacto($grupo = null, $edad, $telefono, $fecha)
    {
        try {

            switch ($edad) {
                case (($edad >= 5 && $edad <= 12) && $grupo == "R"):
                    $grupo = 1;
                    break;
                case (($edad >= 13 && $edad <= 16) && $grupo == "R"):
                    $grupo = 2;
                    break;
                case (($edad >= 17) && $grupo == "R"):
                    $grupo = 3;
                    break;
                case (($edad >= 5 && $edad <= 12) && $grupo == "G"):
                    $grupo = 11;
                    break;
                case (($edad >= 13 && $edad <= 16) && $grupo == "G"):
                    $grupo = 12;
                    break;
                case (($edad >= 17) && $grupo == "G"):
                    $grupo = 13;
                    break;
                default:
                    $grupo = 0;
                    break;
            }

            $user_grupo = ReactogeneidadUser::where(['grupo' => $grupo,'estatus'=>'1'])->get();
            $cantidad = collect($user_grupo)->count();
            if ($cantidad < 1100 && ($grupo == 1 || $grupo == 2 || $grupo == 3)) {
                return $grupo;
            } elseif ($grupo == 1 || $grupo == 2 || $grupo == 3) {
                return $grupo + 10;
            } else {
                return $grupo;
            }
        } catch (\Throwable $th) {
            return response()->json(['errorCode' => 300, 'msj' => 'error en la insercion de datos']);
        }
    }

    public static function ExamenLab($respuestas)
    {
        if(!empty(array_filter($respuestas)))
        {
            $examenLab = array_filter($respuestas);
        }
        else
        {
            $examenLab= '';
        }
        return $examenLab;
    }

    public static function Sintomas($respuestas)
    {
        $sintomas = [];
        if (!empty(array_filter($respuestas))) {
            $sintoma = array_filter($respuestas['sintoma']);           
            $fecha_inicio = array_filter($respuestas['fecha_inicio']); 
            $fecha_fin = array_filter($respuestas['fecha_fin']);           
            $mejoro = array_filter($respuestas['mejoro']); 
            $intensidad = array_filter($respuestas['intensidad']); 
        }
        else{
            $sintoma= '';
            $fecha_inicio= '';
            $fecha_fin= '';
            $mejoro= '';
            $intensidad = '';
        }
        array_push($sintomas,['sintoma' => $sintoma,
                        'fecha_inicio' => $fecha_inicio,
                        'fecha_fin' => $fecha_fin,
                        'mejoro'=>$mejoro,
                        'intensidad' => $intensidad]);
        return $sintomas;
    }

    public static function QuitaAcento($cadena)
    {
        $cadena = str_replace(
            array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
            array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
            $cadena
            );
     
            //Reemplazamos la E y e
            $cadena = str_replace(
            array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
            array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
            $cadena );
     
            //Reemplazamos la I y i
            $cadena = str_replace(
            array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
            array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
            $cadena );
     
            //Reemplazamos la O y o
            $cadena = str_replace(
            array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
            array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
            $cadena );
     
            //Reemplazamos la U y u
            $cadena = str_replace(
            array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
            array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
            $cadena );
     
            //Reemplazamos la N, n, C y c
            $cadena = str_replace(
            array('Ñ', 'ñ', 'Ç', 'ç'),
            array('N', 'n', 'C', 'c'),
            $cadena
            );
            
            return $cadena;
    }

    public static function Medio($medio)
    {
        $medio = strtoupper($medio);
        
        switch ($medio) {
            case 'LLAMADA TELEFONICA':
                return $medio = 1;
                break;
            case 'WHATSAPP':
                return $medio = 2;
                break;
            case 'CORREO ELECTRONICO':
                return $medio = 3;
                break;
            default:
                return 0;
                break;
        }
    }

    public static function DownloadStorage($cod_user,$cuestionario,$url)
    {

        try {
            $fec = new Carbon();
            $fecha = $fec->format('d-m-Y');
            switch ($cuestionario) {
                case 'C4A1':
                    $nameFile = $cod_user."_".$cuestionario."_".$fecha.".jpg";
                    break;
                case 'C4B1':
                    $nameFile = $cod_user."_".$cuestionario."_".$fecha.".jpg";
                    break;
                case 'CR1':
                    $nameFile = $cod_user."_".$cuestionario."_".$fecha.".jpg";
                    break;
                case 'CR1B':
                    $nameFile = $cod_user."_".$cuestionario."_".$fecha.".jpg";
                    break;
                
                default:
                    $nameFile = $cod_user."_".$cuestionario."_".$fecha.".jpg";
                    break;
            }
            
            system("curl -o ".$nameFile." ".$url);

            return Resources::moveFile($nameFile);
        } catch (\Throwable $th) {
            return response()->json(['cod_error' => '500','msg' => 'Error en ejecucion']);
        }
    }

    public function moveFile($nameFile)
    {            
        try {
            $client = new Client([
                'base_uri' => env('API_URL')
            ]);

            $body = [
                'url_name'  => $nameFile,
            ];

            $res = $client->request('GET', '/api/cuadromando/loadImg', [
                'auth' => ['mando_nora', 'mando_4p1n0r4'],
                'headers' => ['Content-Type' => 'application/json'],
                'body' => json_encode($body)
            ]);

            if ($res && $res->getStatusCode() == '200') {
                $res = ($res->getBody()->getContents());
                return $res;
            } else {
                return "nada";
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = (json_decode($e->getResponse()->getBody()->getContents()));
        }
    }

}




?>
