<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class C2Bot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c2-bot', function (Blueprint $table) {
            $table->id();
            $table->string("C2_1",  125)->nullable();
            $table->string("C2_2",  125)->nullable();
            $table->string("C2_2_3",  125)->nullable();
            $table->string("C2_5",  125)->nullable();
            $table->string("C2_5_1",  125)->nullable();
            $table->string("C2_6",  125)->nullable();
            $table->string("C2_6_1",  125)->nullable();
            $table->string("C2_7",  125)->nullable();
            $table->string("C2_7_1",  125)->nullable();
            $table->string("C2_8",  125)->nullable();
            $table->string("C2_8_1",  125)->nullable();
            $table->string("C2_9",  125)->nullable();
            $table->string("C2_9_1",  125)->nullable();
            $table->string("C2_10",  125)->nullable();
            $table->string("C2_10_1",  125)->nullable();
            $table->string("C2_11",  125)->nullable();
            $table->string("C2_11_1",  125)->nullable();
            $table->string("C2_12",  125)->nullable();
            $table->string("C2_12_1",  125)->nullable();
            $table->string("C2_12_2",  125)->nullable();
            $table->string('fecha_operacion',255);
            $table->string('hora',20);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c2-bot');
    }
}
