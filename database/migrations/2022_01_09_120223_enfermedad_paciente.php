<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EnfermedadPaciente extends Migration{
    
    public function up(){
        Schema::create('enfermedad_paciente', function (Blueprint $table) {
            $table->id();
            $table->string('cod_preg_encuesta', 10);
            $table->string('respuesta', 255);
            $table->string('fecha_operacion',255);
            $table->string('hora',20);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    public function down(){
        Schema::dropIfExists('enfermedad_paciente');
    }
}