<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DireccionPaciente extends Migration{
    
    public function up(){
        Schema::create('direccion_paciente', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->string('provincia',50);
            $table->string('distrito',50);
            $table->string('corregimiento',50);
            $table->string('poblado',50);
            $table->string('direccion',255);
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    public function down(){
        Schema::dropIfExists('direccion_paciente');
    }
}