<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SintomasUserCr2 extends Migration{
    
    public function up(){
        Schema::create('sintomasUserCr2', function (Blueprint $table) {
            $table->id();
            $table->string('sintoma',50);
            $table->string('fecha_inicio',20);
            $table->string('fecha_fin',20)->nullable();
            $table->string('mejoro',3);
            $table->string('intensidad',50);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    public function down(){
        Schema::dropIfExists('sintomasUserCr2');
    }
}