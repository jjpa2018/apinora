<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class C4b3Bot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c4b3-bot', function (Blueprint $table) {
            $table->id();
            $table->text("C4B3_1")->nullable();
            $table->text("C4B3_2")->nullable();
            $table->text("C4B3_3")->nullable();
            $table->text("C4B3_3_1")->nullable();
            $table->text("C4B3_3_2")->nullable();
            $table->text("C4B3_4")->nullable();
            $table->text("C4B3_4_1_A")->nullable();
            $table->text("C4B3_4_1_B")->nullable();
            $table->text("C4B3_4_1_C")->nullable();
            $table->text("C4B3_4_1_D")->nullable();
            $table->text("C4B3_4_1_E")->nullable();
            $table->text("C4B3_4_2")->nullable();
            $table->text("C4B3_5")->nullable();
            $table->text("C4B3_5_1")->nullable();
            $table->text("C4B3_6")->nullable();
            $table->text("C4B3_6_1")->nullable();
            $table->text("C4B3_6_2")->nullable();
            $table->text("C4B3_7")->nullable();
            $table->text("C4B3_7_1")->nullable();
            $table->text("C4B3_8")->nullable();
            $table->text("C4B3_9")->nullable();
            $table->text("C4B3_9_1")->nullable();
            $table->text("C4B3_10")->nullable();
            $table->text("C4B3_10_1")->nullable();
            $table->text("C4B3_10_1_1")->nullable();
            $table->text("C4B3_10_1_2")->nullable();
            $table->text("C4B3_10_2")->nullable();
            $table->text("C4B3_10_2_1")->nullable();
            $table->text("C4B3_10_2_2")->nullable();
            $table->text("C4B3_10_3")->nullable();
            $table->text("C4B3_10_3_1")->nullable();
            $table->text("C4B3_10_3_2")->nullable();
            $table->text("C4B3_10_4")->nullable();
            $table->text("C4B3_10_4_1")->nullable();
            $table->text("C4B3_10_4_2")->nullable();
            $table->text("C4B3_11")->nullable();
            $table->text("C4B3_11_1")->nullable();
            $table->text("C4B3_11_2")->nullable();
            $table->text("C4B3_12")->nullable();
            $table->string('fecha_operacion',255);
            $table->string('hora',20);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();
            
            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c4b3-bot');
    }
}
