<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConsultaProfUser extends Migration{
    
    public function up(){
        Schema::create('consulta_prof_user', function (Blueprint $table) {
            $table->id();
            $table->string('medio',50);
            $table->string('dosis',3);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->string('cuestionario',20);
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    public function down(){
        Schema::dropIfExists('consulta_prof_user');
    }
}