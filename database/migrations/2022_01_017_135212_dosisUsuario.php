<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DosisUsuario extends Migration{
    
    public function up(){
        Schema::create('dosisusuario', function (Blueprint $table) {
            $table->id();
            $table->string('dosis',10);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->string('fecha',10);
            $table->string('estatus',2);
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    public function down(){
        Schema::dropIfExists('dosisusuario');
    }
}