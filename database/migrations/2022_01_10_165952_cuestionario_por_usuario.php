<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CuestionarioPorUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuestionario_por_usuario', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->string('cuestionario',20);
            $table->string('fecha',20);
            $table->string('hora',20);
            $table->integer('estatus');
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuestionario_por_usuario');
    }
}
