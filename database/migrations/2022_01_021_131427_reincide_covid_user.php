<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReincideCovidUser extends Migration{
    
    public function up(){
        Schema::create('reincide_covid_user', function (Blueprint $table) {
            $table->id();
            $table->string('recuerda_fecha', 10);
            $table->string('fecha_prueba',20)->nullable();
            $table->string('cuestionario',20);
            $table->string('dosis',3);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        }); 
    }

    public function down(){
        Schema::dropIfExists('reincide_covid_user');
    }
}