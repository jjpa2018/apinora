<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContadoresPorSeguimiento extends Migration{
    
    public function up(){
        Schema::create('contadores_por_seguimiento', function (Blueprint $table) {
            $table->id();
            $table->integer('grupo');
            $table->integer('contador_seguimiento');
            $table->string('tipo_seguimiento',20);
            $table->string('dosis',20);
            $table->string('cuestionario_por_seguimiento',20);
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('contadores_por_seguimiento');
    }
}