<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cr1bBot extends Migration{ 
    
    public function up(){
        Schema::create('cr1b-bot', function (Blueprint $table) {
            $table->id();
            $table->text("CR1B_1")->nullable();
            $table->text("CR1B_2")->nullable();
            $table->text("CR1B_3")->nullable();
            $table->text("CR1B_3_1")->nullable();
            $table->text("CR1B_3_1_1")->nullable();
            $table->text("CR1B_3_2")->nullable();
            $table->text("CR1B_3_2_1")->nullable();
            $table->text("CR1B_3_3")->nullable();
            $table->text("CR1B_3_3_1")->nullable();
            $table->text("CR1B_4")->nullable();
            $table->text("CR1B_5")->nullable();
            $table->text("CR1B_6")->nullable();
            $table->text("CR1B_6_1")->nullable();
            $table->text("CR1B_6_2")->nullable();
            $table->text("CR1B_7")->nullable();
            $table->text("CR1B_7_1")->nullable();
            $table->text("CR1B_8")->nullable();
            $table->text("CR1B_8_1_A")->nullable();
            $table->text("CR1B_8_1_B")->nullable();
            $table->text("CR1B_8_1_C")->nullable();
            $table->text("CR1B_8_1_D")->nullable();
            $table->text("CR1B_8_1_E")->nullable();
            $table->text("CR1B_8_2")->nullable();
            $table->text("CR1B_9")->nullable();
            $table->text("CR1B_9_1")->nullable();
            $table->text("CR1B_10")->nullable();
            $table->text("CR1B_11")->nullable();
            $table->text("CR1B_11_1")->nullable();
            $table->text("CR1B_11_2")->nullable();
            $table->text("CR1B_12")->nullable();
            $table->text("CR1B_12_1")->nullable();
            $table->text("CR1B_13")->nullable();
            $table->text("CR1B_13_1")->nullable();
            $table->text("CR1B_13_2")->nullable();
            $table->text("CR1B_14")->nullable();
            $table->text("CR1B_14_1")->nullable();
            $table->string('fecha_operacion',255);
            $table->string('hora',20);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    public function down(){
        Schema::dropIfExists('cr1b-bot');
    }
}