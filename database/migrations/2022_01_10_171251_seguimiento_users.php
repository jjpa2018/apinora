<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeguimientoUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguimiento_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->string('cuestionario', 20);
            $table->string('fecha_seguimiento',20);
            $table->string('dosis',20);
            $table->integer('tipo_seguimiento');// diario, semanal , mensual , bimensual, etc
            $table->integer('estatus');
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seguimiento_users');
    }
}
