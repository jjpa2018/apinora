<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class C5Bot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c5-bot', function (Blueprint $table) {
            $table->id();
            $table->text("C5_1")->nullable();
            $table->text("C5_2")->nullable();
            $table->text("C5_3")->nullable();
            $table->text("C5_3_1")->nullable();
            $table->text("C5_3_1_1")->nullable();
            $table->text("C5_3_2")->nullable();
            $table->text("C5_3_2_1")->nullable();
            $table->text("C5_3_3")->nullable();
            $table->text("C5_3_3_1")->nullable();
            $table->text("C5_4")->nullable();
            $table->text("C5_5")->nullable();
            $table->text("C5_6")->nullable();
            $table->text("C5_6_1")->nullable();
            $table->text("C5_6_2")->nullable();
            $table->text("C5_7")->nullable();
            $table->text("C5_7_1_A")->nullable();
            $table->text("C5_7_1_B")->nullable();
            $table->text("C5_7_1_C")->nullable();
            $table->text("C5_7_1_D")->nullable();
            $table->text("C5_7_1_E")->nullable();
            $table->text("C5_7_1")->nullable();
            $table->text("C5_7_2")->nullable();
            $table->text("C5_8")->nullable();
            $table->text("C5_8_1")->nullable();
            $table->text("C5_9")->nullable();
            $table->text("C5_9_1")->nullable();
            $table->text("C5_9_2")->nullable();
            $table->text("C5_10")->nullable();
            $table->text("C5_10_1")->nullable();
            $table->text("C5_11")->nullable();
            $table->text("C5_12")->nullable();
            $table->text("C5_12_1")->nullable();
            $table->text("C5_13")->nullable();
            $table->text("C5_13_1")->nullable();
            $table->text("C5_13_1_1")->nullable();
            $table->text("C5_13_1_2")->nullable();
            $table->text("C5_13_2")->nullable();
            $table->text("C5_13_2_1")->nullable();
            $table->text("C5_13_2_2")->nullable();
            $table->text("C5_13_3")->nullable();
            $table->text("C5_13_3_1")->nullable();
            $table->text("C5_13_3_2")->nullable();
            $table->text("C5_13_4")->nullable();
            $table->text("C5_13_4_1")->nullable();
            $table->text("C5_13_4_2")->nullable();
            $table->text("C5_14")->nullable();
            $table->text("C5_14_1")->nullable();
            $table->text("C5_14_2")->nullable();
            $table->text("C5_15")->nullable();
            $table->string('fecha_operacion',255);
            $table->string('hora',20);
            $table->unsignedBigInteger('idpersona') ->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c5-bot');
    }
}
