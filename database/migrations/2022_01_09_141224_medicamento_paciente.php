<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MedicamentoPaciente extends Migration{
    
    public function up(){
        Schema::create('medicamento_paciente', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->string('medicamento',20);
            $table->string('uso',20);
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    public function down(){
        Schema::dropIfExists('medicamento_paciente');
    }
}