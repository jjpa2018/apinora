<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Persona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona', function (Blueprint $table) {
            $table->id();
            $table->string('tiene_pasaporte',10);
            $table->string('doc_identifica',15);
            $table->string('cod_unico',15);
            $table->string('nombre_participante',150);
            $table->string('fecha_nacimiento',10);
            $table->integer('edad')->nullable();
            $table->string('telefono',20);
            $table->string('conector_pau',20);
            $table->string('correo_informante',50)->nullable();
            $table->integer('grupo')->nullable();
            $table->string('estatus',2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona');
    }
}
