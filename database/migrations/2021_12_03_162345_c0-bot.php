<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class C0Bot extends Migration
{
    /**
     * Run the migrations_
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c0-bot', function (Blueprint $table) {
            $table->id();
            $table->string("C0_1", 125)->nullable();
            $table->string("C0_1_1", 125)->nullable();
            $table->string("C0_1_2", 125)->nullable();
            $table->string("C0_1_3", 125)->nullable();
            $table->string("C0_1_3_1", 125)->nullable();
            $table->string("C0_1_4", 125)->nullable();
            $table->string("C0_2", 125)->nullable();
            $table->string("C0_2_1", 125)->nullable();
            $table->string("C0_2_2", 125)->nullable();
            $table->string("C0_3", 125)->nullable();
            $table->string("C0_4", 125)->nullable();
            $table->string("C0_5", 125)->nullable();
            $table->string("C0_5_1", 125)->nullable();
            $table->string("C0_5_2", 125)->nullable();
            $table->string("C0_6", 125)->nullable();
            $table->string("C0_7", 125)->nullable();
            $table->string("C0_8", 125)->nullable();
            $table->string("C0_8_1", 125)->nullable();
            $table->string('fecha_operacion',25)->nullable();
            $table->string('hora',25)->nullable();
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations_
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c0-bot');
    }
}
