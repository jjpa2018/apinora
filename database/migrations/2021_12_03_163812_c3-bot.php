<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class C3Bot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c3-bot', function (Blueprint $table) {
            $table->id();
            $table->string("C3_1",  125)->nullable();
            $table->string("C3_2",  125)->nullable();
            $table->string("C3_3",  125)->nullable();
            $table->string("C3_3_1",  125)->nullable();
            $table->string("C3_4",  125)->nullable();
            $table->string("C3_5",  125)->nullable();
            $table->string("C3_5_1",  125)->nullable();
            $table->string("C3_6",  125)->nullable();
            $table->string("C3_6_1",  125)->nullable();
            $table->string("C3_7",  125)->nullable();
            $table->string("C3_8",  125)->nullable();
            $table->string("C3_9",  125)->nullable();
            $table->string("C3_9_1",  125)->nullable();
            $table->string("C3_9_2",  125)->nullable();
            $table->string('fecha_operacion',255);
            $table->string('hora',20);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c3-bot');
    }
}
