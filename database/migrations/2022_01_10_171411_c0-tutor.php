<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class C0Tutor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c0-tutor', function (Blueprint $table) {
            $table->id();
            $table->string('cedula_panama',5);
            $table->string('cedula_informante',15);
            $table->string('nombre_representante',150);
            $table->string('fecha_nacimiento_r',10);
            $table->string('prentesco',10);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c0-tutor');
    }
}
