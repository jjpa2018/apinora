<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MedicamentoPostVacunaUser extends Migration{
    
    public function up(){
        Schema::create('medicamento_post_vacuna_user', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->string('medicamento',20);
            $table->string('uso',20);
            $table->string('recomendado_por',20);
            $table->timestamps();
            $table->string('cuestionario',20);

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    public function down(){
        Schema::dropIfExists('medicamento_post_vacuna_user');
    }
}