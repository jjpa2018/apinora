<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class C4a2Bot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c4a2-bot', function (Blueprint $table) {
            $table->id();
            $table->text("C4A2_1")->nullable();
            $table->text("C4A2_2")->nullable();
            $table->text("C4A2_3")->nullable();
            $table->text("C4A2_3_1")->nullable();
            $table->text("C4A2_3_1_1")->nullable();
            $table->text("C4A2_3_2")->nullable();
            $table->text("C4A2_3_2_1")->nullable();
            $table->text("C4A2_3_3")->nullable();
            $table->text("C4A2_3_3_1")->nullable();
            $table->text("C4A2_4")->nullable();
            $table->text("C4A2_5")->nullable();
            $table->text("C4A2_6")->nullable();
            $table->text("C4A2_6_1")->nullable();
            $table->text("C4A2_6_2")->nullable();
            $table->text("C4A2_7")->nullable();
            $table->text("C4A2_7_1_A")->nullable();
            $table->text("C4A2_7_1_B")->nullable();
            $table->text("C4A2_7_1_C")->nullable();
            $table->text("C4A2_7_1_D")->nullable();
            $table->text("C4A2_7_1_E")->nullable();
            $table->text("C4A2_7_2")->nullable();
            $table->text("C4A2_8")->nullable();
            $table->text("C4A2_8_1")->nullable();
            $table->text("C4A2_9")->nullable();
            $table->text("C4A2_9_1")->nullable();
            $table->text("C4A2_9_2")->nullable();
            $table->text("C4A2_10")->nullable();
            $table->text("C4A2_10_1")->nullable();
            $table->text("C4A2_11")->nullable();
            $table->text("C4A2_12")->nullable();
            $table->text("C4A2_12_1")->nullable();
            $table->text("C4A2_13")->nullable();
            $table->text("C4A2_13_1")->nullable();
            $table->text("C4A2_13_1_1")->nullable();
            $table->text("C4A2_13_1_2")->nullable();
            $table->text("C4A2_13_2")->nullable();
            $table->text("C4A2_13_2_1")->nullable();
            $table->text("C4A2_13_2_2")->nullable();
            $table->text("C4A2_13_3")->nullable();
            $table->text("C4A2_13_3_1")->nullable();
            $table->text("C4A2_13_3_2")->nullable();
            $table->text("C4A2_13_4")->nullable();
            $table->text("C4A2_13_4_1")->nullable();
            $table->text("C4A2_13_4_2")->nullable();
            $table->text("C4A2_14")->nullable();
            $table->text("C4A2_14_1")->nullable();
            $table->text("C4A2_14_2")->nullable();
            $table->text("C4A2_15")->nullable();
            $table->string('fecha_operacion',255);
            $table->string('hora',20);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c4a2-bot');
    }
}
