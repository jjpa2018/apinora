<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExamenLabUser extends Migration{
    
    public function up(){
        Schema::create('examenLabUser', function (Blueprint $table) {
            $table->id();
            $table->string('cod_preg_encuesta', 10);
            $table->string('examen',50);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->string('cuestionario',20);
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('examenLabUser');
    }
}