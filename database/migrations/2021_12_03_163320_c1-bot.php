<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class C1Bot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c1-bot', function (Blueprint $table) {
            $table->id();
            $table->string("C1_1",  125)->nullable();
            $table->string("C1_1_1",  125)->nullable();
            $table->string("C1_3",  125)->nullable();
            $table->string("C1_3_1",  125)->nullable();
            $table->string("C1_3_2",  125)->nullable();
            $table->string("C1_3_3",  125)->nullable();
            $table->string("C1_4",  125)->nullable();
            $table->string("C1_4_1",  125)->nullable();
            $table->string("C1_5",  125)->nullable();
            $table->string("C1_5_1",  125)->nullable();
            $table->string("C1_5_2",  125)->nullable();
            $table->string("C1_5_3",  125)->nullable();
            $table->string("C1_5_4",  125)->nullable();
            $table->string("C1_6",  125)->nullable();
            $table->string("C1_7",  125)->nullable();
            $table->string("C1_8",  125)->nullable();
            $table->string("C1_9",  125)->nullable();
            $table->string("C1_9_1",  125)->nullable();
            $table->string("C1_10",  125)->nullable();
            $table->string("C1_10_1",  125)->nullable();
            $table->string("C1_11",  125)->nullable();
            $table->string("C1_11_1_A",  125)->nullable();
            $table->string("C1_11_1_B",  125)->nullable();
            $table->string("C1_11_1_C",  125)->nullable();
            $table->string("C1_11_1_D",  125)->nullable();
            $table->string("C1_11_1_E",  125)->nullable();
            $table->string("C1_11_1_F",  125)->nullable();
            $table->string("C1_11_1_G",  125)->nullable();
            $table->string("C1_11_1_H",  125)->nullable();
            $table->string("C1_11_1_I",  125)->nullable();
            $table->string("C1_11_1_J",  125)->nullable();
            $table->string("C1_11_1_K",  125)->nullable();
            $table->string("C1_11_1_L",  125)->nullable();
            $table->string("C1_12",  125)->nullable();
            $table->string("C1_13",  125)->nullable();
            $table->string("C1_14",  125)->nullable();
            $table->string("C1_15",  125)->nullable();
            $table->string("C1_17",  125)->nullable();
            $table->string("C1_18",  125)->nullable();
            $table->string("C1_19",  125)->nullable();
            $table->string("C1_20",  125)->nullable();
            $table->string("C1_21",  125)->nullable();
            $table->string("C1_21_1",  125)->nullable();
            $table->string("C1_22",  125)->nullable();
            $table->string("C1_22_1",  125)->nullable();
            $table->string("C1_23",  125)->nullable();
            $table->string("C1_23_1",  125)->nullable();
            $table->string("C1_23_2",  125)->nullable();
            $table->string("C1_24",  125)->nullable();
            $table->string("C1_24_1_1",  125)->nullable();
            $table->string("C1_24_1_1_1",  125)->nullable();
            $table->string("C1_24_1_2",  125)->nullable();
            $table->string("C1_24_1_2_1",  125)->nullable();
            $table->string("C1_24_1_3",  125)->nullable();
            $table->string("C1_24_1_3_1",  125)->nullable();
            $table->string("C1_24_1_4",  125)->nullable();
            $table->string("C1_24_1_4_1",  125)->nullable();
            $table->string("C1_25",  125)->nullable();
            $table->string("C1_25_1",  125)->nullable();
            $table->string("C1_25_2",  125)->nullable();
            $table->string("C1_25_3",  125)->nullable();
            $table->string("C1_25_4",  125)->nullable();
            $table->string("C1_25_5",  125)->nullable();
            $table->string('fecha_operacion',255);
            $table->string('hora',20);
            $table->unsignedBigInteger('idpersona')->nullable();
            $table->timestamps();

            $table->foreign('idpersona')
                  ->references('id')->on('persona')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c1-bot');
    }
}
