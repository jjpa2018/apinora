<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContadoresPorNotificaciones extends Migration{
    
    public function up(){
        Schema::create('contadores_por_notificaciones', function (Blueprint $table) {
            $table->id();
            $table->integer('grupo');
            $table->integer('contador_notificacion');
            $table->string('tipo_seguimiento',20);
            $table->string('dosis',20);
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('contadores_por_notificaciones');
    }
}